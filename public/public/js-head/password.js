$(function() {

	/*
	| -------------------------------------------------------------------------
	| timeline
	| -------------------------------------------------------------------------
	*/

	application.prototype.password = function () {
		var $this = this;

		$this.password.init = function() {
			// post
			$application.form.validate($('form#password'), {
				'email': {
					required: true,
					email: true
				}
			}, {
				'email': {
					required: 'Você precisa informar seu e-mail',
					email: 'Você precisa informar um e-mail válido'
				}
			});


			// newPassword
			$application.form.validate($('form#newPassword'), {
				'password': {
					required: true
				},
				'passwordComparison': {
					required: true,
					equalTo: "#password"
				}
			}, {
				'password': {
					required: 'Você precisa definir uma nova senha'
				},
				'passwordComparison': {
					required: 'Você precisa informar sua nova senha novamente',
					equalTo: 'As senhas informadas não são iguais'
				}
			});
		};

		$this.password.init();
	};
});