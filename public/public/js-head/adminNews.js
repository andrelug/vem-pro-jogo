$(function() {

	/*
	| -------------------------------------------------------------------------
	| adminNews
	| -------------------------------------------------------------------------
	*/

	application.prototype.adminNews = function () {
		var $this = this;

		$this.adminNews.init = function() {
			// clear form createNews
			$('a[data-target=#createNewsModal]').click(function() {
				$.each($('form#createNews').find('.form-group.has-error'), function() {
					$(this).find('.error-placement').remove();
					$(this).removeClass("has-error");
				});
			});

			// clear form updateNews
			$('a[data-target=#updateNewsModal]').click(function() {
				$.each($('form#updateNews').find('.form-group.has-error'), function() {
					$(this).find('.error-placement').remove();
					$(this).removeClass("has-error");
				});
			});

			// createNews
			$application.form.validate($('form#createNews'), {
				'title': {
					required: true
				},
				'subtitle': {
					required: true
				},
				'categoryId': {
					required: true
				},
				'content': {
					required: true
				}

			}, {
				'title': {
					required: 'Você precisa definir um título'
				},
				'subtitle': {
					required: 'Você precisa definir um subtítulo'
				},
				'categoryId': {
					required: 'Você precisa escolher uma categoria'
				},
				'content': {
					required: 'Você não pode criar uma notícia sem texto'
				}
			});

			// updateNews
			$application.form.validate($('form#updateNews'), {
				'title': {
					required: true
				},
				'subtitle': {
					required: true
				},
				'categoryId': {
					required: true
				},
				'content': {
					required: true
				}

			}, {
				'title': {
					required: 'Você precisa definir um título'
				},
				'subtitle': {
					required: 'Você precisa definir um subtítulo'
				},
				'categoryId': {
					required: 'Você precisa escolher uma categoria'
				},
				'content': {
					required: 'Você não pode criar uma notícia sem texto'
				}
			});
		};

		$this.adminNews.init();
	};
});
