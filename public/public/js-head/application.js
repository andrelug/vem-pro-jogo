$(function() {

	/*
	| -------------------------------------------------------------------------
	| application
	| -------------------------------------------------------------------------
	*/

	application = function($options) {
		this.options = {
			debug: true,
			touch: 'ontouchstart' in document.documentElement ? true : false
		}

		this.options = $.extend({}, this.options, $options);

		this.form();
		// this.resize();
		// this.slide();
	};

	/*
	| -------------------------------------------------------------------------
	| log
	| -------------------------------------------------------------------------
	*/

	application.prototype.log = function($message) {
		var $this = this;

		if ($this.options.debug === true) {
			if (typeof console == 'object') {
				console.log($message)
			}
		}
	};

	/*
	| -------------------------------------------------------------------------
	| form
	| -------------------------------------------------------------------------
	*/

	application.prototype.form = function() {

		var $this = this;

		/*
		| -------------------------------------------------------------------------
		| forms > mask
		| -------------------------------------------------------------------------
		*/

		// date
		$('input.date').mask('99/99/9999');
		// time
		$('input.time').mask('99:99');
		// phone
		$('input.phone').mask("(99) 99999999?9");

		/*
		| -------------------------------------------------------------------------
		| forms > validate custom methods
		| -------------------------------------------------------------------------
		*/

		$.validator.addMethod('phone', function(value, element) {
			return value === '' ? true : value.match(/^\([0-9]{2}?\)\s([0-9]{8}?|[0-9]{9}?)$/);
		});

		$.validator.addMethod('postal_code', function(value, element) {
			return value.match(/^[0-9]{5}\-[0-9]{3}$/);
		});

		/*
		| -------------------------------------------------------------------------
		| form > validate
		| -------------------------------------------------------------------------
		*/

		$this.form.validate = function($form, $rules, $messages) {
			$form.validate({
				focusInvalid: false,
				onfocusout: false,
				onkeyup: false,
				focusCleanup: false,
				ignore: [],
				rules: $rules,
				messages: $messages,
				highlight: function($element, $error, $valid) {
					var $form_group = $($element).parents('.form-group');

					$form_group.addClass("has-error");
				},
				unhighlight: function($element, $error, $valid) {
					var $form_group = $($element).parents('.form-group.has-error');

					$form_group.find('.error-placement').remove();

					$form_group.removeClass("has-error");
				},
				errorPlacement: function($error, $element) {
					var $form_group = $($element).parents('.form-group.has-error');

					$form_group.find('.error-placement').remove();

					$form_group.append('<p class="help-block error-placement">' + $error.text() + '</p>');
				}
			});
		};
	};

	// /*
	// | -------------------------------------------------------------------------
	// | resize
	// | -------------------------------------------------------------------------
	// */

	// application.prototype.resize = function() {

	// 	var $this = this;

	// 	$this.resize.init = function() {

	// 		$this.resize.time = new Date(1, 1, 2000, 12);
	// 		$this.resize.timeout = false;
	// 		$this.resize.delta = 200;

	// 		$(window).resize($.proxy(function($e) {
	// 			$this.resize.time = new Date();

	// 			if ($this.resize.timeout === false) {
	// 				$this.resize.timeout = true;
	// 				// $this.resize.overlay(true);

	// 				setTimeout($this.resize.end, this.delta);
	// 			}
	// 		}, this));

	// 		$(window).resize();
	// 	};

	// 	$this.resize.overlay = function($show) {
	// 		var $body = $('body');

	// 		if ($show === true) {
	// 			$body.addClass('resize overlay');
	// 		} else if ($show === false) {
	// 			$body.removeClass('resize overlay');
	// 		}
	// 	};

	// 	$this.resize.end = function() {
	// 		if((new Date() - $this.resize.time) < $this.resize.delta) {
	// 			setTimeout($this.resize.end, $this.resize.delta);
	// 		} else {
	// 			$this.resize.timeout = false;
	// 			// $this.resize.overlay(false);

	// 			// resize methods
	// 			// $this.home.resize();
	// 		}

	// 	};

	// 	$this.resize.init();
	// }

	// /*
	// | -------------------------------------------------------------------------
	// | slide
	// | -------------------------------------------------------------------------
	// */

	// application.prototype.slide = function() {

	// 	var $this = this;

	// 	$this.slide.init = function() {
	// 		$slide = $('section.slide-full, section.slide-half');

	// 		if($slide.length) {
	// 			$slide.img    = $slide.find('img');
	// 			$slide.scroll = $slide.find('.scroll');

	// 			var $slides = [];

	// 			$.each($slide.img, function($i, $item){
	// 				$slides.push($item.src);
	// 				$item.remove();
	// 			});

	// 			$slide.backstretch($slides, {duration: 3000, fade: 750});

	// 			// bounce scroll icon
	// 			$this.slide.bounce = function() {
	// 				$slide.scroll.effect('bounce', {
	// 					times: 1,
	// 					distance: 20
	// 				}, 1000, $this.slide.bounce);
	// 			};
	// 			$this.slide.bounce();
	// 		}
	// 	};

	// 	$this.slide.init();
	// };
});