/**
 * MessageController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var processMessages = function(user, messages){
  return Promise.map(messages, function(message){
    message.user = user.id === message.owner.id ? message.to : message.owner;
    return message;
  });
};

module.exports = {

  last: function(req, res){
    var query = {
        where: {
          or: [
            { owner: req.user.id },
            { to: req.user.id }
          ]
        }
      };

    Message.findOne(query).sort('createdAt DESC').limit(1).then(function(message){
      var route = message ? '/messages/'+message.owner+'*'+message.to : '/messages/all';
      res.redirect(route);
    });
  },

	list: function (req, res) {
    var uid = req.user.id;

    var id = req.param('id').split('*');
    var route = '/messages/'+req.param('id');
    var sendTo = _.find(id, function(item){ return item !== uid; });

    var query = { where: { or: [{ owner: id[0], to: id[1] }, { owner: id[1], to: id[0] }] } };

    var lastMessageP = Message
      .find({ or: [{ owner: id[0], to: id[1] }, { owner: id[1], to: id[0] }] })
      .populate('owner')
      .populate('to')
      .sort('createdAt ASC')
      .then(function(results){
        return Promise.map(results, function(message){
          message = message.toJSON();
          message.formatedCreated = moment(message.createdAt).format('DD [de] MMMM [de] YYYY [as] HH:mm');
          message.isMe = uid === message.owner.id ? true : false;
          // message.user = uid === message.owner.id ? message.to : message.owner;
          message.user = message.owner;
          return message;
        });
      });

    var messagesP = Message.find({or: [{ to: uid }, {owner: uid}]})
      .populate('owner')
      .populate('to')
      .sort('createdAt DESC')
      .then(function(results){
        return Promise.map(results, function(message){
          message.group = [message.owner.id, message.to.id].sort().join('*');
          message.formatedCreated = moment(message.createdAt).format('DD [de] MMMM [de] YYYY [as] HH:mm');
          message.content = message.content.length >= 50 ? message.content.substring(0, 50) + "..." : message.content;
          return message;
        });
      })
      .then(function(results){
        return _.uniq(results, 'group');
      })
      .then(function(results){
        return processMessages(req.user, results);
      })
      .then(function(messages) {
        return _.chain(messages)
          .groupBy('group')
          .pairs()
          .map(function(currentItem){
            return _.object(_.zip(['identifier', 'content'], currentItem));
          })
          .value();
      })
      .then(function(results){
        return Promise.map(results, function(message){
          message = message.content[0];
          return message;
        });
      });

    Promise.join(lastMessageP, messagesP).spread(function(last, messages){
      res.render('messages/index', {
        model: messages,
        activeMessage: last,
        sendTo: sendTo,
        route: route,
        flash: req.flash('message')
      });
    })

  },

  create: function(req, res){
    var body = req.body;
    var route = body.route;

    Message.create(body).then(function(message){
      Notification
        .create({ type: 'messageReceived', contextType: 'message', contextId: message.id, owner: message.owner, user: message.to })
        .then(function(result){
          sails.io.sockets.emit('user %@ notification'.fmt(message.to), {
            id: result.id,
            verb: 'created',
            data: result
          });
          res.redirect(route);
        });
    });

  }
};

