/**
 * NotificationController
 *
 * @description :: Server-side logic for managing notifications
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var processNotification = function(user, notifications){
  return Promise.map(notifications, function(notification){
    notification.read = true;
    return notification.save().then(function(){
      notification = notification.toJSON();
      notification.user = user.id === notification.owner.id ? notification.user : notification.owner;
      notification.createdAt = moment(notification.createdAt).format('DD [de] MMMM [de] YYYY [às] HH:mm');

      notification.isComment = notification.type === 'comment';
      notification.isFriendAccepted = notification.type === 'friendAccepted';
      notification.isFriendRequest = notification.type === 'friendRequest';

      notification.isInvite = notification.type === 'inviteToProject';
      notification.isAccepted = notification.type === 'acceptedToProject';
      notification.isInviteAccepted = notification.type === 'inviteAcceptedToProject';
      notification.isRequestedToProject = notification.type === 'requestedToProject';
      notification.isMessageReceived = notification.type === 'messageReceived';
      notification.postLiked = notification.type === 'postLiked';
      notification.commentLiked = notification.type === 'commentLiked';

      var Model = sails.models[notification.contextType];
      return Model.findOneById(notification.contextId).then(function(context){
        notification.context = context;
        return notification;
      });
    });


  });
};

module.exports = {
	find: function(req, res){
    var uid = req.user.id;

    res.locals.isMe = true;
    Notification
      .find({user: uid})
      .populate('owner')
      .populate('user')
      .sort('createdAt DESC')
      .then(function(notifications){
        return processNotification(req.user, notifications);
      })
      .then(function(notifications){
        res.render('notifications/index', {
          notifications: notifications,
          notificationsUnread: 0,
          flash: req.flash('message')
        });
      });
  }
};

