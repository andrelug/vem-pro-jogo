/**
 * NewstickerController
 *
 * @description :: Server-side logic for managing newstickers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	find: function(req, res){
    var currentUser = req.user
      , search = req.param('search').value
      , skip = req.param('start')
      , limit = req.param('length');

    var query = search ? { name: { 'like': '%'+search+'%' } } : {};

    var totalUsers = Newsticker.count(query);

    var Model = Newsticker.find(query);

    Model = Model.skip(skip).limit(15);
    
    Promise.join(totalUsers, Model).spread(function(total, results){
      return Promise.map(results, function(result){
        result = result.toJSON();

        return _.chain(result)
          .pick('id', 'url')
          .values()
          .value();
      }).then(function(results){
        var obj = {
          "draw": req.param('draw'),
          "recordsTotal": total,
          "recordsFiltered": total,
          "data": results
        }
        res.set('Content-Type', 'text/html');
        res.jsonp(obj);
      });
      
    })
  },

  create: function(req, res){
    var body = req.body;
    console.log(body);
    req.file('img').upload({
      dirname: require('path').join(sails.config.appPath, '/assets/assets/newsticker/')
    },function (err, uploadedFiles) {
      if (err) return res.negotiate(err);

      body.img = uploadedFiles[0].fd.replace(sails.config.appPath+'/assets', '');

      Newsticker.create(body).then(function(){
        res.redirect('/admin/newsticker');
      });
    });
  },

  delete: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id;
    var newstickerId = req.param('id');

    Newsticker.findOne(newstickerId).then(function(newsticker){
      Newsticker.destroy({id: newstickerId})
      .exec(function(err){
        if (err) return res.negotiate(err);

        // emit to user
        sails.io.sockets.emit('user %@ newsticker'.fmt(uid), {
          id: newsticker.id,
          verb: 'deleted',
          data: newsticker
        });

        // send 200 response
        return res.ok(newsticker);
      });
    });
  },

  img: function (req, res){
    req.validate({
      id: 'string'
    });

    Newsticker.findOne(req.param('id')).exec(function (err, newsticker){
      if (err) return res.negotiate(err);
      if (!newsticker) return res.notFound();

      // User has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!newsticker.img) return res.notFound();

      var img = require('path').join(sails.config.appPath, '/assets/', newsticker.img);

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(img).on('error', function (err){ return res.serverError(err); }).pipe(res);
    });
  }
};

