/**
 * ContactController
 *
 * @description :: Server-side logic for managing contacts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  index: function(req, res){
    res.render('contact', {
      flash: req.flash('message')
    });
  },

	create: function(req, res){
    var body = req.body;
    var nodemailer = require('nodemailer');
    var sgTransport = require('nodemailer-sendgrid-transport');

    var options = {
      auth: {
        api_user: 'alvaromenezes',
        api_key: 'V3mt1m@pass'
      }
    }

    var transporter = nodemailer.createTransport(sgTransport(options));

    //reference the plugin
    var hbs = require('nodemailer-express-handlebars');
    //attach the plugin to the nodemailer transporter
    transporter.use('compile', hbs({
      viewPath: require('path').join(sails.config.appPath, '/views/emails/'),
      extName: '.hbs'
    }));

   User.findOneById(req.user.id).then(function(user){
      transporter.sendMail({
        from: user.name+' <'+user.email+'>',
        to: 'vemprojogo@legassessoria.com.br',
        subject: 'Dúvidas, sugestões ou reclamações - #VEMPROJOGO! TIM',
        template: 'contact',
        context: {
          name: user.name,
          email: user.email,
          registrationNumber: user.registrationNumber,
          area: user.area,
          subarea: user.subarea,
          message: body.message
        }
      }, function(err, result){
        req.flash('message', 'Mensagem enviada com sucesso!')
        res.redirect('/contact');
      });
    });
  }
};

