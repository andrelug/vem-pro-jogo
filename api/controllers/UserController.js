/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	index: function(req, res){
    var currentUser = req.user
      , search = req.param('search')
      , skip = req.param('skip')
      , limit = req.param('limit');

    var query = search ? { or: [
        { name: { 'like': '%'+search+'%' } },
        { email: { 'like': '%'+search+'%' } }
      ],
      id: { '!': currentUser.id }
    } : {};

    var totalUsers = User.count(query);

    var Model = User.find(query);

    if(skip && limit) Model = Model.paginate({page: skip, limit: limit});

    Promise.join(totalUsers, Model).spread(function(total, users){
      if(skip && limit) users.push({meta: {total: total / limit}})
      res.ok(users);
    })
  },

  find: function(req, res){
    var currentUser = req.user
      , search = req.param('search').value
      , skip = req.param('start')
      , limit = req.param('length');

      // console.log(skip, req.param)

    var query = search ? { or: [
        { name: { 'like': '%'+search+'%' } },
        { email: { 'like': '%'+search+'%' } }
      ],
      id: { '!': currentUser.id }
    } : {};

    var totalUsers = User.count(query);

    var Model = User.find(query);

    Model = Model.skip(skip).limit(15);

    Promise.join(totalUsers, Model).spread(function(total, users){
      return Promise.map(users, function(result){
        result.area = result.area || '';
        result.subarea = result.subarea || '';
        return _.chain(result)
          .pick('registrationNumber', 'name', 'email', 'area', 'subarea', 'id')
          .values()
          .value();
      }).then(function(users){
        var obj = {
          "draw": req.param('draw'),
          "recordsTotal": total,
          "recordsFiltered": total,
          "data": users
        }
        res.set('Content-Type', 'text/html');
        res.send(''+req.param('callback')+'('+JSON.stringify(obj)+')');
      });

    })
  },

  active: function(req, res){
    var user = req.user;
    var body = req.body;

    // if(!body.password || !body.passwordComparison) delete body.password;

    User.findOneById(req.user.id).then(function(user){
      _.extend(user, body);
      user.save().then(function(user){
        req.flash('message', 'Perfil editado com sucesso');
        res.redirect('/about');
      });
    });
  },

  update: function(req, res){
    var user = req.user;
    var body = req.body;

    if(!body.password || !body.passwordComparison) delete body.password;


    User.findOneById(req.user.id).then(function(user){
      _.extend(user, body);
      user.save().then(function(user){
        req.flash('message', 'Perfil editado com sucesso');
        res.redirect('/about');
      });
    });
  },

  avatar: function(req, res){
    var body = req.body;

    fs = require('fs');

    req.file('avatar').upload({
      dirname: process.cwd() + '/assets/assets/avatar/'
    },function (err, uploadedFiles) {
      if (err) return res.negotiate(err);

      if(uploadedFiles[0]){
        gm(uploadedFiles[0].fd)
          .resize(300 + '>')
          .gravity('Center')
          .crop(200,200)
          .write(uploadedFiles[0].fd, function (err) {
            // if (!err) console.log('crazytown has arrived');

            User.findOneById(req.user.id).then(function(user){
              if(uploadedFiles[0]) user.avatar = uploadedFiles[0].fd.replace(sails.config.appPath+'/assets', '');

              var filename = uploadedFiles[0].fd.substring(uploadedFiles[0].fd.lastIndexOf('/')+1);
              var uploadLocation = process.cwd() +'//assets/assets/avatar/' + filename;
              var tempLocation = process.cwd() + '/.tmp/public/assets/avatar/' + filename;
              fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));

              user.save().then(function(user){
                req.flash('message', 'Avatar atualizado com sucesso');
                setTimeout(function(){
                  res.redirect('/timeline');
                }, 1000)
              });
            });

          });
      } else {
        req.flash('message', 'Voce precisa enviar um arquivo');
        res.redirect('/timeline');
      }
    });
  },

  adminFindOne: function(req, res){
    User.findOneById(req.param('id')).then(function(user){
      res.render('admin/users/read', {
        model: user.toJSON(),
        flash: req.flash('message')
      });
    });
  },

  profile: function(req, res){
    User.findOneById(req.session.user).then(function(user){
      if(user.active == false) {
        res.render('profile/active', {
          model: user.toJSON(),
          updateProfile: false,
          flash: req.flash('message')
        });
      } else {
        res.render('profile/edit', {
          model: user.toJSON(),
          updateProfile: false,
          flash: req.flash('message')
        });
      }
    });
  },

  forgotView: function(req, res){
    res.render('password', {
      flash: req.flash('message')
    });
  },

  forgot: function(req, res){
    var body = req.body;
    var nodemailer = require('nodemailer');
    var sgTransport = require('nodemailer-sendgrid-transport');

    var options = {
        auth: {
            api_user: 'insightbh',
            api_key: 'Insight2@'
        }
    }

    var transporter = nodemailer.createTransport(sgTransport(options));

    //reference the plugin
    var hbs = require('nodemailer-express-handlebars');
    //attach the plugin to the nodemailer transporter
    transporter.use('compile', hbs({
      viewPath: require('path').join(sails.config.appPath, '/views/emails/'),
      extName: '.hbs'
    }));

    User.findOneByEmail(body.email).then(function(user){

      user.resetKey = user.id;

      user.save().then(function(user){
        transporter.sendMail({
          from: '#VEMPROJOGO! <noreply@vemprojogo.com.br>',
          to: user.email,
          subject: 'Recuperação de senha',
          template: 'reset',
          context: {
            name: user.name,
            url: 'http://www.vemprojogotim.com.br/reset_password/'+user.id
          }
        }, function(err, result){
          req.flash('message', 'Confira sua caixa de entrada, você receberá um e-mail com o procedimento necessário para redefinir sua senha.')

          res.redirect('password');
        });
      });
    });
  },

  resetPasswordView: function(req, res){
    res.render('new-password', {
      token: req.param('token'),
      flash: req.flash('message')
    });
  },

  resetPassword: function(req, res){
    var body = req.body;

    User.findOne({resetKey: req.param('token')}).then(function(user){
      user.resetKey = null;
      user.password = body.password;
      user.passwordComparison = body.passwordComparison;

      return user.save();
    }).then(function(){
      req.flash('message', 'Sua senha foi alterada com sucesso!');
      res.redirect('login');
    });
  }

};

