var bcrypt = require('bcrypt');
/**
 * AuthController
 *
 * @module      :: Controller
 * @description :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
  view: function(req, res){
    res.render('login', {
      hideNavBarNews: true,
      flash: req.flash('message'),
      unAuthenticated: true
    });
  },

  login: function(req, res) {
    var bcrypt = require('bcrypt');
    var identification = req.body.username;

    User.findOne({or: [{cpf: identification}, {registrationNumber: identification}, {email: identification}]}).exec(function (err, user) {
      if (err) res.badRequest({ error: err });

      if (user) {
        console.log(user.status, user.role);
        var allowedStatus = ['Atividade Normal', 'Férias']

        console.log(_.contains(allowedStatus, user.status))

        if(!_.contains(allowedStatus, user.status) && user.role !== 'admin') {
          req.flash('message', 'Usuário imposibilitado de realizar login por motivo de afastamento'),
          res.redirect('/login');
          return;
        }

        bcrypt.compare(req.body.password, user.password, function (err, match) {
          if (err) res.badRequest({ error: err });

          if (match) {
            // password match
            req.session.user = user.id;
            res.redirect('/timeline')
          } else {
            // invalid password
            if (req.session.user) req.session.user = null;
            req.flash('message', 'Usuário ou senha incorreta'),
            res.redirect('/login');
          }
        });
      } else {
        req.flash('message', 'Usuário ou senha incorreta'),
        res.redirect('/login');
      }
    });

  },

  logout: function(req, res) {
    // req.logout();
    delete req.session.user;
    res.redirect('/');
  }
};
