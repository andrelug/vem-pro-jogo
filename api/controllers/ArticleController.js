/**
 * ArticleController
 *
 * @description :: Server-side logic for managing articles
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	find: function(req, res){
    var skip = req.param('skip')
      , limit = req.param('limit');

    var Model = Article.find().where({ sort: 'createdAt DESC' }).populate('categoryId');

    if(skip && limit) Model = Model.paginate({page: skip, limit: limit});

    var categoriesP = Category.find();
    var articlesP = Model;

    Promise.join(categoriesP, articlesP).spread(function(categories, articles){
      res.render('admin/news/index', {
        model: articles,
        categories: categories,
        flash: req.flash('message')
      });
    });
  },

  findOne: function(req, res){

    var lastArticleP = Article.find().where({ sort: 'createdAt DESC' }).populate('categoryId').paginate({page: 1, limit: 6});

    var articleP = Article.findOne(req.params.id).populate('categoryId');

    Promise.join(lastArticleP, articleP).spread(function(lastArticles, article){
      res.render('news/show', {
        model: article,
        lastArticles: lastArticles,
        flash: req.flash('message'),
        layout: 'clean'
      });
    });
  },

  create: function(req, res){
    var body = req.body;
    
    req.file('photo').upload({
      dirname: require('path').join(sails.config.appPath, '/assets/files/')
    },function (err, uploadedFiles) {
      if (err) return res.negotiate(err);

      body.photo = uploadedFiles[0].fd.replace(sails.config.appPath+'/assets', '');

      Article.create(body).then(function(){
        res.redirect('/admin/news');
      });
    });
  },

  update: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var body = req.body;
    var uid = req.user.id;
    var articleId = req.param('id');

    
    req.file('photo').upload({
      dirname: require('path').join(sails.config.appPath, '/assets/files/')
    },function (err, uploadedFiles) {
      if (err) return res.negotiate(err);

      if(uploadedFiles[0]) body.photo = uploadedFiles[0].fd.replace(sails.config.appPath+'/assets', '');

      Article.update(articleId, req.body)
        .then(function(article){
          res.redirect('/admin/news');
        });
      });
  },

  delete: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id;
    var articleId = req.param('id');

    Article.findOne(articleId).then(function(article){
      Article.destroy({id: article.id})
      .exec(function(err){
        if (err) return res.negotiate(err);

        // emit to user
        sails.io.sockets.emit('user %@ article'.fmt(uid), {
          id: article.id,
          verb: 'deleted',
          data: article
        });

        // send 200 response
        return res.ok(article);
      });
    });
  }
};

