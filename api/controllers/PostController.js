/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  create: function(req, res){
    var body = req.body;
    body.owner = req.user.id;
    
    req.file('filePhoto').upload({
      dirname: require('path').join(sails.config.appPath, '/assets/assets/post/photo/')
    },function (err, uploadedFiles) {
      if (err) return res.negotiate(err);

      if(uploadedFiles[0]){
        gm(uploadedFiles[0].fd)
          .resize(666 + '>')
          .write(uploadedFiles[0].fd, function (err) {
            Post.create(body).then(function(post){
              post.photo = uploadedFiles[0].fd.replace(sails.config.appPath+'/assets', '');

              post.save().then(function(post){
                res.redirect('/timeline');
              });
            });
          });
      } else {
        Post.create(body).then(function(){
          res.redirect('/timeline');
        });
      }
    });
  },

  img: function (req, res){
    req.validate({
      id: 'string'
    });

    Post.findOne(req.param('id')).exec(function (err, newsticker){
      if (err) return res.negotiate(err);
      if (!newsticker) return res.notFound();

      // User has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!newsticker.photo) return res.notFound();

      var photo = require('path').join(sails.config.appPath, '/assets/', newsticker.photo);

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(photo).on('error', function (err){ return res.serverError(err); }).pipe(res);
    });
  },

  show: function(req, res){
    var id = req.params.id;
    var uid = req.user.id;

    Post.findOneById(id).populate('likes').populate('owner').then(function(post){
      return PostLike.count({post: post.id, owner: uid}).then(function(isLiked){
        post.isLiked = !!isLiked;
        return post;
      });
    }).then(function(post){
      var obj = {};
      return Comment.find({postId: post.id}).where({ sort: 'createdAt DESC' }).populate('owner').populate('likes').then(function(results){
        return Promise.map(results, function(result){
          result = result.toJSON();
          return CommentLike.count({comment: result.id, owner: uid}).then(function(isLiked){
            result.isLiked = !!isLiked;
            
            if(post.owner === uid){
              result.canDelete = true;
            } else if (result.owner.id === uid) {
              result.canDelete = true;
            } else {
              result.canDelete = false;
            }

            result.isMe = result.owner.id === uid ? true : false;
            return result;
          });
        });
      }).then(function(comments){
        post = post.toJSON();
        post.comments = comments;
        post.isMe = post.owner === uid ? true : false;
        return post;
      });
    }).then(function(post){
      res.render('profile/timeline-post-show', {
        model: post,
        flash: req.flash('message')
      });
    });
  },

  like: function(req, res){
    var body = req.body;
    var id = req.params.id;

    Post.findOneById(id).then(function(post){
      post.likes.add(body);
      return post.save();
    }).then(res.ok);
  },

  dislike: function(req, res){
    PostLike.destroy(req.body).then(res.ok);
  },

  likes: function(req, res){
    var id = req.params.id;
    PostLike
      .find({post: id})
      .populate('owner')
      .then(function(likes){
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');  
        res.render('profile/timeline-post-likes', {
          model: likes,
          flash: req.flash('message'),
          layout: 'modal'
        });
      })
  }

};

