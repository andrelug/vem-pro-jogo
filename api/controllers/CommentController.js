/**
 * CommentController
 *
 * @description :: Server-side logic for managing comments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	create: function(req, res){
    var body = req.body;

    if(body.body){
      Comment.create(body).then(function(){
        res.redirect('/timeline');
      });
    } else {
      req.flash('message', 'Você precisa escrever um comentário');
      res.redirect('/timeline');
    }
    
  },

  delete: function(req, res){
    var commentId = req.param('id');
    var uid = req.user.id;

    Comment.findOne(commentId).populate('postId').then(function(comment){
      Comment.destroy({id: comment.id})
      .exec(function(err){
        if (err) return res.negotiate(err);

        // emit to user
        sails.io.sockets.emit('user %@ comment'.fmt(comment.owner), {
          id: comment.id,
          verb: 'deleted',
          data: comment
        });

        // emit to other logged in user for sync status
        sails.io.sockets.emit('user %@ comment'.fmt(uid), {
          id: comment.id,
          verb: 'deleted',
          data: comment
        });

        // send 200 response
        return res.ok(comment);
      });
    });
  },

  like: function(req, res){
    var body = req.body;
    var id = req.params.id;

    Comment.findOneById(id).then(function(comment){
      comment.likes.add(body);
      return comment.save();
    }).then(res.ok);
  },

  dislike: function(req, res){
    CommentLike.destroy(req.body).then(res.ok);
  },

  likes: function(req, res){
    var id = req.params.id;
    CommentLike
      .find({comment: id})
      .populate('owner')
      .then(function(likes){
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');  
        res.render('profile/timeline-comment-likes', {
          model: likes,
          flash: req.flash('message'),
          layout: 'modal'
        });
      })
  }
};

