/**
 * CategoryController
 *
 * @description :: Server-side logic for managing categories
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  find: function(req, res){
    var categoiesP = Category.find().populate('articles', { sort: 'createdAt DESC', limit: 3 });
    var newstickerP = Newsticker.find();
    
    Promise.join(categoiesP, newstickerP).spread(function(categories, newsticker){
      res.render('news/index', {
        model: categories,
        newsticker: newsticker,
        flash: req.flash('message'),
        layout: 'clean'
      });
    });
  },
	findOne: function(req, res){
    Category
      .findOne(req.params.id)
      .populate('articles', { sort: 'createdAt DESC' })
      .then(function(category){
        res.render('news/category', {
          model: category,
          flash: req.flash('message'),
          layout: 'clean'
        });
      });
  },

  adminFind: function(req, res){
    var model = Category
      .find()
      .populate('articles', { sort: 'createdAt DESC' })
      // .then(res.ok);
      .then(function(categories){
        res.render('admin/categories', {
          model: categories
        });
      });

  },
};

