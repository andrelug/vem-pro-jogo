/**
 * ProfileController
 *
 * @description :: Server-side logic for managing profiles
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	about: function(req, res){
    res.locals.isMe = true;
    res.render('profile/about', {
        flash: req.flash('message')
    });
  },

  friendAbout: function(req, res){
    var uid = req.param('id');

    var userP = User.findOneById(uid).then(function(user){
      user = user.toJSON();
      res.locals.userSelected = user;
      res.locals.isUserSelected = true;
      res.locals.notUpdateAvatar = true;
      res.locals.notUpdateProfile = true;
      return user;
    });

    var currentUserFriendsP = new Promise(function(resolve){
      Friend.getUserFriends(uid, function(err, friends){
        Promise.map(friends, function(item){
          var result = _.pick(item, 'from', 'to');
          return _.values(result)
        }).then(function(Ids){
          return _.flatten(Ids);
        }).then(function(Ids){
          return _.uniq(Ids);
        }).then(function(Ids){
          return _.without(Ids, uid.id);
        }).then(function(Ids){
          resolve(Ids);
        });
      });
    });

    Promise.join(currentUserFriendsP, userP).spread(function(friends, user){
      res.locals.isMyFriend = _.contains(friends, user.id);
      res.render('profile/about', {
        userSelected: user,
        notUpdateProfile: true,
        showSendMessage: true,
        flash: req.flash('message')
      });
    });
  },

};

