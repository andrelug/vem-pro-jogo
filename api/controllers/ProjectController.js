/**
 * ProjectController
 *
 * @description :: Server-side logic for managing projects
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var processFriends = function(user, friends){
  return Promise.map(friends, function(friend){

    friend.user = user.id === friend.from.id ? friend.to : friend.from;
    return friend;
  });
};


module.exports = {

  index: function(req, res){
    var uid = req.user.id;

    var friendsP = Friend.find({status: 'accepted', or: [{ from: uid },{ to: uid }]})
      .populate('from')
      .populate('to')
      .then(function(friends){
        return processFriends(req.user, friends);
      });

    var projectsP = Project.find({status: ['Inscrito', 'Habilitado', 'Aprovado', 'Em andamento', 'Encerrado', 'Descontinuado']}).populate('owner').populate('secondaryTenderer').then(function(results){
      return Promise.map(results, function(result){
        result = result.toJSON();

        return Participant.find({status: 'Aceito', project: result.id}).then(function(participants){
          result.participants = participants
          return result;
        });
      });
    });

    var myProjectsP = Participant
      .find({user: uid, status: 'Aceito'})
      .then(function(participants){
        return _.chain(participants)
                .map(function(item){
                  return _.values(_.pick(item, 'project'));
                })
                .flatten()
                .value();
      })
      .then(function(participants){
        return Promise.map(participants, function(id){
          return Project.findOneById(id).populate('owner').populate('secondaryTenderer');
        });
      })
      .then(function(projects){
        return Promise.map(projects, function(project){
          project = project.toJSON();
          return Participant.find({status: 'Aceito', project: project.id}).then(function(participants){
            project.participants = participants
            if(project.status === 'Inscrito') project.flashInfo = '<strong>Projeto pendente!</strong> Por enquanto seu projeto está pendente, dentro de alguns dias o comitê gestor irá analisar se seu projeto está habilitado para participar do programa #VEMPROJOGO. * Os projetos inscritos no #VEMPROJOGO! passarão, inicialmente, por um processo de triagem em que será verificado o atendimento às solicitações do regulamento.'
            if(project.status === 'Recusado') project.flashDangerRecusado = '<strong>Projeto recusado!</strong> Infelizmente seu projeto não estáva apto para participar do programa #VEMPROJOGO.'
            if(project.status === 'Encerrado') project.flashSuccess = '<strong>Projeto concluído!</strong> Seu projeto foi concluído com sucesso.'
            if(project.status === 'Descontinuado') project.flashDangerDescontinuado = '<strong>Projeto descontinuado!</strong> Seu projeto foi descontinuado.'
            return project;
          });
        });
      });

    var friendsListP = friendsP.then(function(friends){
      return Promise.map(friends, function(friend){
        friend = friend.toJSON();
        return Participant
          .find({user: friend.user.id, status: 'Aceito'})
          .then(function(participants){
            friend.projectCount = participants.length;
            return friend;
          });
      }).then(function(friends){
        return _.reject(friends, function(friend){
          return friend.projectCount == 2;
        });
      });
    });

    var requestsP = Participant.find({user: uid, status: 'Convite'}).then(function(requests){
      return Promise.map(requests, function(result){
        return Project.findOneById(result.project).populate('owner').populate('secondaryTenderer').then(function(project){
          project = project.toJSON();
          return Participant.find({status: 'Aceito', project: project.id}).then(function(participants){
            project.participants = participants
            return project;
          });
        });
      }).then(_.flatten);
    });

    Promise.join(friendsListP, projectsP, myProjectsP, requestsP).spread(function(friends, projects, myProjects, requests){

      res.locals.isMe = true;
      res.render('projects/index', {
        model: projects,
        myProjects: myProjects,
        requests: requests,
        friends: friends,
        hideInscription: myProjects.length < 2 ? false : true,
        flash: req.flash('message')
      });
    });
  },

  find: function(req, res){
    var currentUser = req.user
      , search = req.param('search').value
      , skip = req.param('start')
      , limit = req.param('length');

    var query = search ? { name: { 'like': '%'+search+'%' } } : {};

    var totalUsers = Project.count(query);

    var Model = Project.find(query);

    Model = Model.skip(skip).limit(15);

    Promise.join(totalUsers, Model).spread(function(total, results){
      return Promise.map(results, function(result){
        result = result.toJSON();

        return _.chain(result)
          .pick('name', 'startDateFormated', 'endDateFormated', 'state', 'city', 'status', 'id')
          .values()
          .value();
      }).then(function(results){
        var obj = {
          "draw": req.param('draw'),
          "recordsTotal": total,
          "recordsFiltered": total,
          "data": results
        }
        res.set('Content-Type', 'text/html');
        res.send(req.param('callback')+'('+JSON.stringify(obj)+')');
      });

    })
  },

  friendFind: function(req, res){
    var uid = req.param('id');

    var userP = User.findOneById(uid).then(function(user){
      user = user.toJSON();
      res.locals.userSelected = user;
      res.locals.isUserSelected = true;
      res.locals.notUpdateAvatar = true;
      res.locals.notUpdateProfile = true;
      return user;
    });

    var currentUserFriendsP = new Promise(function(resolve){
      Friend.getUserFriends(uid, function(err, friends){
        Promise.map(friends, function(item){
          var result = _.pick(item, 'from', 'to');
          return _.values(result)
        }).then(function(Ids){
          return _.flatten(Ids);
        }).then(function(Ids){
          return _.uniq(Ids);
        }).then(function(Ids){
          return _.without(Ids, uid.id);
        }).then(function(Ids){
          resolve(Ids);
        });
      });
    });

    var myProjectsP = Participant
      .find({user: uid, status: 'Aceito'})
      .then(function(participants){
        return _.chain(participants)
                .map(function(item){
                  return _.values(_.pick(item, 'project'));
                })
                .flatten()
                .value();
      })
      .then(function(participants){
        return Promise.map(participants, function(id){
          return Project.findOne({id: id, status: ['Inscrito', 'Habilitado', 'Aprovado', 'Em andamento', 'Encerrado', 'Descontinuado']}).populate('owner').populate('secondaryTenderer');
        });
      })
      .then(function(projects){

        return Promise.map(_.compact(projects), function(project){
          project = project.toJSON();
          return Participant.find({status: 'Aceito', project: project.id}).then(function(participants){ 
            project.participants = participants
            if(project.status === 'Inscrito') project.flashInfo = '<strong>Projeto pendente!</strong> Por enquanto seu projeto está pendente, dentro de alguns dias o comitê gestor irá analisar se seu projeto está habilitado para participar do programa #VEMPROJOGO. * Os projetos inscritos no #VEMPROJOGO! passarão, inicialmente, por um processo de triagem em que será verificado o atendimento às solicitações do regulamento.'
            if(project.status === 'Recusado') project.flashDangerRecusado = '<strong>Projeto recusado!</strong> Infelizmente seu projeto não estáva apto para participar do programa #VEMPROJOGO.'
            if(project.status === 'Encerrado') project.flashSuccess = '<strong>Projeto concluído!</strong> Seu projeto foi concluído com sucesso.'
            if(project.status === 'Descontinuado') project.flashDangerDescontinuado = '<strong>Projeto descontinuado!</strong> Seu projeto foi descontinuado.'
            return project;
          });
        });
      });

    Promise.join(currentUserFriendsP, myProjectsP, userP).spread(function(friends, myProjects, user){
      res.locals.isMyFriend = _.contains(friends, user.id);
      res.render('projects/index', {
        myProjects: myProjects,
        userSelected: user,
        hide: true,
        showSendMessage: true,
        flash: req.flash('message')
      });
    })

  },

  adminFindOne: function(req, res){
    Project.findOneById(req.param('id')).populate('owner').populate('secondaryTenderer').then(function(project){
      Participant
        .find({project: project.id, status: 'Aceito'})
        .populate('user')
        .then(function(results){
          return Promise.map(results, function(result){
            result = result.toJSON();
            return _.chain(result).pick('user').values().value();
          }).then(_.flatten);
        })
        .then(function(participants){
          project = project.toJSON();
          project.participants = participants;
          res.render('admin/projects/read', {
            model: project,
            flash: req.flash('message')
          });
        });
    });
  },

  show: function(req, res){
    var uid = req.user.id;

    var friendsP = Friend.find({status: 'accepted', or: [{ from: uid },{ to: uid }]})
      .populate('from')
      .populate('to')
      .then(function(friends){
        return processFriends(req.user, friends);
      });

    var projectP = Project.findOneById(req.param('id')).populate('owner').populate('secondaryTenderer');

    var participantsP = projectP.then(function(project){
      return Participant.find({project: project.id}).then(function(results){
          return Promise.map(results, function(result){
          result = result.toJSON();

          return _.chain(result).pick('user').values().value();
        }).then(_.flatten);
      });
    })

    var requestsP = projectP.then(function(project){
      return Participant.find({project: project.id, status: 'Pedido'}).populate('user');
    });

    var acceptedP = projectP.then(function(project){
      return Participant
        .find({project: project.id, status: 'Aceito'})
        .populate('user')
        .then(function(accepts){
          return Promise.map(accepts, function(item){
            item.canDelete = project.owner.id == item.user.id || project.secondaryTenderer.id == item.user.id ? false : true;
            return item;
          });
        });
    });

    var inviteP = projectP.then(function(project){
      return Participant.find({project: project.id, status: 'Convite'}).populate('user');
    });

    var participantP = projectP.then(function(project){
      return Participant.findOne({user: uid, project: project.id}).then(function(participant){
        res.locals.requested = participant && participant.status === 'Pedido';
        res.locals.showActivate = participant && participant.status === 'Convite';
        res.locals.participateIn = project.owner.id !== uid && project.secondaryTenderer.id !== uid  && participant && participant.status === 'Aceito';
        res.locals.canParticipate = project.owner.id === uid || project.secondaryTenderer.id === uid || project.status === 'Recusaso' || project.status === 'Descontinuado' || project.status === 'Encerrado' || project.status === 'Em andamento' ? false : true;


        res.locals.showParticipated = project.owner.id === uid || project.secondaryTenderer.id === uid  && participant ? true : false;

        Participant.find({user: uid, status: 'Aceito'})
          .then(function(participants){
            if(participants.length == 2) res.locals.canParticipate = false;
          });

        Participant.find({project: project.id, status: 'Aceito'})
          .then(function(participants){
            if(participants.length == parseInt(project.maxParticipants)) {
              res.locals.canParticipate = false;
              res.locals.inviteFriends = true;
            }
          });
      });
    });



    Promise.join(friendsP, projectP, requestsP, acceptedP, inviteP, participantP, participantsP).spread(function(friends, project, requests, accepts, invites, participant, participants){
      friends = _.reject(friends, function(item){
        return _.contains(participants, item.user.id);
      });

      project = project.toJSON();


      project.participants = accepts;
      res.render('projects/show', {
          model: project,
          friends: friends,
          requests: requests,
          accepts: accepts,
          invites: invites,
          showParticipantsAndInvite: project.owner.id === uid || project.secondaryTenderer.id === uid ? true : false,
          flash: req.flash('message')
        });
    });
  },

  participant: function(req, res){
    var body = req.body;
    var projectId = req.param('id')
    var participantId = req.param('participantId')
    var action = req.param('action')

    if(action === 'aceitar'){
      Participant
        .findOne({user: participantId, project: projectId})
        .then(function(participant){
          participant.status = 'Aceito';
          participant.save().then(function(record){
            Project.findOneById(projectId)
              .then(function(project){
                Notification
                  .create({ type: 'inviteAcceptedToProject', contextType: 'project', contextId: project.id, owner: record.user, user: project.owner })
                  .then(function(project){
                    sails.io.sockets.emit('user %@ notification'.fmt(project.user), {
                      id: project.id,
                      verb: 'created',
                      data: project
                    });
                    res.redirect(body.route);
                  });
              });
          });
        });
    } else if (action === 'participar'){
      Project.findOneById(projectId).then(function(project){
        project.participants.add({project: projectId, user: participantId, status: 'Pedido'});
        project.save().then(function(){
            Notification
              .create({ type: 'requestedToProject', contextType: 'project', contextId: project.id, owner: participantId, user: project.owner })
              .then(function(project){
                sails.io.sockets.emit('user %@ notification'.fmt(project.owner), {
                  id: project.id,
                  verb: 'created',
                  data: project
                });
                res.redirect(body.route);
              });
          });
      });
    } else if (action === 'aprovado'){
      Participant
        .findOne({user: participantId, project: projectId})
        .then(function(participant){
          participant.status = 'Aceito';
          participant.save().then(function(record){

            Project.findOneById(projectId)
              .then(function(project){
                Notification
                  .create({ type: 'acceptedToProject', contextType: 'project', contextId: project.id, owner: project.owner, user: record.user })
                  .then(function(project){
                    sails.io.sockets.emit('user %@ notification'.fmt(project.user), {
                      id: project.id,
                      verb: 'created',
                      data: project
                    });
                    res.redirect(body.route);
                  });
              });
          });
        });
    } else if (action === 'recusado'){
      Participant
        .destroy({user: participantId, project: projectId}).then(function(){
          res.redirect(body.route);
        });
    }
  },

  participants: function(req, res){
    var body = req.body;
    var projectP = Project.findOneById(req.param('id'));
    var participants = [];

    projectP.then(function(project){
      if(body.participants instanceof Array){
        body.participants.forEach(function(id){
          participants.push({user: id, project: project.id})
        });
      } else {
        participants.push({user: body.participants, project: project.id})
      }

      project.participants.add(participants);
      return project.save();
    }).then(function(){
      res.redirect(body.route);
    })
  },

  create: function(req, res){
    var body = req.body;
    var participants = [];

    req.file('costSpreadsheet').upload({
      dirname: require('path').join(sails.config.appPath, '/assets/files')
    },function (err, uploadedFiles) {
      if (err) return res.negotiate(err);

      body.costSpreadsheet = uploadedFiles[0].fd.replace(sails.config.appPath+'/assets', '');

      Project.create(body).then(function(project){

        participants.push({user: body.owner, project: project.id, status: 'Aceito'});
        participants.push({user: body.secondaryTenderer, project: project.id, status: 'Aceito'});

        body.participants.forEach(function(id){
            participants.push({user: id, project: project.id})
        });

        project.participants.add(participants);

        project.save().then(function(){
          res.redirect('/projects');
        });
      });
    });
  },

  update: function(req, res){
    var body = req.body;

    Project.findOneById(body.id).then(function(project){
      project.status = body.status;
      project.save().then(function(){
        res.redirect(req.param('route'));
      });
    });
  }
};

