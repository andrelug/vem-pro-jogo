/**
 * FriendController.js
 *
 * @description ::
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */


var processFriends = function(user, friends){
  return Promise.map(friends, function(friend){
    friend = friend.toJSON();
    friend.user = user.id === friend.from.id ? friend.to : friend.from;
    friend.userInverse = user.id !== friend.from.id ? friend.to : friend.from;
    friend.showAccept = friend.to.id === user.id ? true : false;
    return friend;
  });
};

module.exports = {
  find: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id;

    var query = { or: [{ from: uid },{ to: uid }] };

    var search = _.merge(actionUtil.parseCriteria(req, ['owner']), query);


    var friendsP = Friend.find(_.extend({status: 'accepted'}, query))
      .populate('from')
      .populate('to')
      .then(function(friends){
        if (sails.onlineusers) {
          for(var i = friends.length - 1; i >= 0; i--) {
            if (friends[i].from == uid) {
              if(sails.onlineusers[friends[i].to]) friends[i].onlineStatus = 'online';
            } else {
              if(sails.onlineusers[friends[i].from]) friends[i].onlineStatus = 'online';
            }
          }
        }
        return processFriends(req.user, friends);
      });

    var friendsRequestsP = Friend
      .find({
        status: 'requested',
        or: [{
          from: uid,
        },{
          to: uid
        }]
      })
      .populate('from')
      .populate('to')
      .then(function(friends){
        return processFriends(req.user, friends);
      });

    var friendIds = friendsP.then(function(friends){
      return Promise.map(friends, function(item){
        
        return _.chain(item).pick('user').values().first().pick('id').values().first().value();
      });
    });

    var userSearchP = req.param('search') && User.find({ id: { '!': uid }, name: { 'like': '%'+req.param('search')+'%' }}).sort('name ASC')
      .then(function(users){
        return friendIds.then(function(friends){
          return _.reject(users, function(user){
            return _.contains(friends, user && user.id);
          });
        })
      });

    Promise.join(friendsP, friendsRequestsP, userSearchP).spread(function(friends, requests, searchResults){

      searchResults && searchResults.forEach(function(user){
        var requestIds = _.chain(requests).pluck('user').pluck('id').value();
        user.hasInvited = _.contains(requestIds, user.id) ?  true : false;
      });

      searchResults = _.reject(searchResults, function(user){
        var userRequested = _.find(requests, function(item){
          return item.user.id == user.id;
        });

        return userRequested && !userRequested.showAccept;
      });

      res.locals.isMe = true;
      res.render('friends/index', {
        friends: friends,
        requests: requests,
        searchResults: searchResults,
        flash: req.flash('message')
      });
    }).catch(res.negotiate);
  },

  viewOnlyFriends: function(req, res){
    var uid = req.param('id');

    var userP = User.findOneById(uid).then(function(user){
      user = user.toJSON();
      res.locals.userSelected = user;
      res.locals.isUserSelected = true;
      res.locals.notUpdateAvatar = true;
      res.locals.notUpdateProfile = true;
      return user;
    });

    var friendsP = Friend.find({status: 'accepted', or: [{ from: uid },{ to: uid }]})
      .populate('from')
      .populate('to')
      .then(function(friends){
        return userP.then(function(user){
          return processFriends(user, friends);
        })
      })

    var currentUserFriendsP = new Promise(function(resolve){
      Friend.getUserFriends(uid, function(err, friends){
        Promise.map(friends, function(item){
          var result = _.pick(item, 'from', 'to');
          return _.values(result)
        }).then(function(Ids){
          return _.flatten(Ids);
        }).then(function(Ids){
          return _.uniq(Ids);
        }).then(function(Ids){
          return _.without(Ids, uid.id);
        }).then(function(Ids){
          resolve(Ids);
        });
      });
    });

    Promise.join(currentUserFriendsP, friendsP, userP).spread(function(fiendIds, friends, user){
      res.locals.isMyFriend = _.contains(fiendIds, user.id);
      res.render('friends/index', {
        friends: friends,
        notUpdateProfile: true,
        showSendMessage: true,
        hide: true,
        flash: req.flash('message')
      });
    });
  },

  onlyFriends: function(req, res){
    var uid = req.user.id;

    Friend.find({status: 'accepted', or: [{ from: uid },{ to: uid }]})
      .populate('from')
      .populate('to')
      .then(function(friends){
        return processFriends(req.user, friends);
      })
      .then(function(friends){
        return res.ok(friends);
      })
  },

  findOne: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id
      , friendId = req.param('friendId');

    Friend.getUsersRelationship(uid, friendId, function(err, friend){
      if (err) return res.negotiate(err);
      if(!friend) return res.ok({});
      return res.ok(friend);
    });
  },

  /**
   * Request friend relationship
   */
  create: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id
      , friendId = req.body.to
      , obj = { from: uid, to: friendId }
      , friendExistP = Friend.findOne(obj);

    var friendP = friendExistP.then(function(friendExist){
      if(friendExist) return friendExist;
      return Friend.create(obj);
    });

    friendP.then(function(friend){
      // emit to user
      sails.io.sockets.emit('user %@ friend'.fmt(friend.to), {
        id: friend.id,
        verb: 'created',
        data: friend
      });
      // emit to other logged in user for sync status
      sails.io.sockets.emit('user %@ friend'.fmt(friend.from), {
        id: friend.id,
        verb: 'created',
        data: friend
      });

      // notify
      sails.emit('api:model:friend:requested', friend, req.user);
      // send result
      res.redirect('/friends#friendsRequests')
    })
    .catch(res.negotiate);
  },

  update: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id;
    var friendId = req.param('id');
    var body = req.body;
    // first get and check if has one relationship
    Friend.findOne(friendId).then(function(friend){
      if(!friend) return res.notFound();
      // only logged in user can accept one friend
      if(friend.to != uid ) return res.forbiden();

      // set new status
      friend.status = req.body.status;
      friend.save(function(err){
        if (err) return res.negotiate(err);

        // emit to user
        sails.io.sockets.emit('user %@ friend'.fmt(friend.to), {
          id: friend.id,
          verb: 'updated',
          data: friend
        });

        // emit to other logged in user for sync status
        sails.io.sockets.emit('user %@ friend'.fmt(friend.from), {
          id: friend.id,
          verb: 'updated',
          data: friend
        });

        // notify
        sails.emit('api:model:friend:accepted', friend, req.user);
        // send the response
        return res.ok(friend);
      });
    })
    .catch(res.negotiate);
  },

  accept: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id;
    var friendId = req.param('id');
    var body = req.body;
    // first get and check if has one relationship
    Friend.findOne({
      or: [
        {from: uid, to: friendId},
        {to: uid, from: friendId},
      ]
    }).then(function(friend){
      if(!friend) return res.notFound();
      // only logged in user can accept one friend
      if(friend.to != uid ) return res.forbiden();

      // set new status
      friend.status = req.body.status;
      friend.save(function(err){
        if (err) return res.negotiate(err);
        friend = friend.toJSON();

        friend.user = uid === friend.from.id ? friend.to : friend.from;

        // emit to user
        sails.io.sockets.emit('user %@ friend'.fmt(friend.to), {
          id: friend.id,
          verb: 'updated',
          data: friend
        });

        // emit to other logged in user for sync status
        sails.io.sockets.emit('user %@ friend'.fmt(friend.from), {
          id: friend.id,
          verb: 'updated',
          data: friend
        });

        // notify
        sails.emit('api:model:friend:accepted', friend, req.user);
        // send the response
        return res.ok(friend);
      });
    })
    .catch(res.negotiate);
  },

  delete: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id;
    var friendId = req.param('id');

    Friend.findOne({
      or: [
        {from: req.user.id, to: friendId},
        {from: friendId, to: req.user.id}
      ]
    }).then(function(friend){
      Friend.destroy({id: friend.id})
      .exec(function(err){
        if (err) return res.negotiate(err);

        // emit to user
        sails.io.sockets.emit('user %@ friend'.fmt(friend.to), {
          id: friend.id,
          verb: 'deleted',
          data: friend
        });

        // emit to other logged in user for sync status
        sails.io.sockets.emit('user %@ friend'.fmt(friend.from), {
          id: friend.id,
          verb: 'deleted',
          data: friend
        });

        // send 200 response
        return res.ok(friend);
      });
    });
  }
};
