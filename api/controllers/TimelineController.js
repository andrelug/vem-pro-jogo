/**
 * TimelineController
 *
 * @description :: Server-side logic for managing timelines
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  index: function(req, res){
    var uid = req.user.id;

    var friendsP = new Promise(function(resolve){
      Friend.getUserFriends(req.user.id, function(err, friends){
        Promise.map(friends, function(item){
          var result = _.pick(item, 'from', 'to');
          return _.values(result)
        }).then(function(Ids){
          return _.flatten(Ids);
        }).then(function(Ids){
          return _.uniq(Ids);
        }).then(function(Ids){
          resolve(Ids);
        });
      });
    });

    var timelinesP = friendsP.then(function(Ids){

      return Timeline.find().where({ or: [{owner: Ids}, {owner: uid}] }).populate('owner').sort({ createdAt: 'desc' })
    });

    var processTimelineP = timelinesP.then(function(results){
      return Promise.map(results, function(result){
        result = result.toJSON();
        var Model = sails.models[result.contextType];
        Model = Model.findOneById(result.contextId).populate('likes');

        return Model.then(function(contextResult){
          return PostLike.count({post: contextResult.id, owner: uid}).then(function(isLiked){
            contextResult.isLiked = !!isLiked;
            return contextResult;
          });
        }).then(function(contextResult){
          var obj = {};
          return Comment.find({postId: contextResult.id}).where({ sort: 'createdAt DESC' }).populate('owner').populate('likes').then(function(results){
            return Promise.map(results, function(result){
              result = result.toJSON();
              return CommentLike.count({comment: result.id, owner: uid}).then(function(isLiked){
                result.isLiked = !!isLiked;
                
                if(contextResult.owner === uid){
                  result.canDelete = true;
                } else if (result.owner.id === uid) {
                  result.canDelete = true;
                } else {
                  result.canDelete = false;
                }

                result.isMe = result.owner.id === uid ? true : false;
                return result;
              });
            });
          }).then(function(comments){
            obj[result.contextType] = contextResult;
            obj[result.contextType].comments = comments;
            obj[result.contextType].isMe = contextResult.owner === uid ? true : false;
            return _.extend(result, obj);
          });
        })
      });
    });

    processTimelineP.then(function(results){
      res.locals.isMe = true;
      res.render('profile/timeline', {
        model: results,
        flash: req.flash('message')
      });
    });
  },

  myPosts: function(req, res){
    var uid = req.user.id;

    var processTimelineP = Timeline.find({owner: uid}).populate('owner').sort({ createdAt: 'desc' }).then(function(results){
      return Promise.map(results, function(result){
        result = result.toJSON();
        var Model = sails.models[result.contextType];
        Model = Model.findOneById(result.contextId).populate('likes');

        return Model.then(function(contextResult){
          return PostLike.count({post: contextResult.id, owner: uid}).then(function(isLiked){
            contextResult.isLiked = !!isLiked;
            return contextResult;
          });
        }).then(function(contextResult){
          var obj = {};
          return Comment.find({postId: contextResult.id}).where({ sort: 'createdAt DESC' }).populate('owner').populate('likes').then(function(results){
            return Promise.map(results, function(result){
              result = result.toJSON();
              return CommentLike.count({comment: result.id, owner: uid}).then(function(isLiked){
                result.isLiked = !!isLiked;
                
                if(contextResult.owner === uid){
                  result.canDelete = true;
                } else if (result.owner.id === uid) {
                  result.canDelete = true;
                } else {
                  result.canDelete = false;
                }

                result.isMe = result.owner.id === uid ? true : false;
                return result;
              });
            });
          }).then(function(comments){
            obj[result.contextType] = contextResult;
            obj[result.contextType].comments = comments;
            obj[result.contextType].isMe = contextResult.owner === uid ? true : false;
            return _.extend(result, obj);
          });
        })
      });
    });

    processTimelineP.then(function(results){
      res.locals.isMe = true;
      res.render('profile/timeline', {
        model: results,
        flash: req.flash('message')
      });
    });
  },

  friendTimeline: function(req, res){
    var uid = req.param('id');

    var friendsP = new Promise(function(resolve){
      Friend.getUserFriends(uid, function(err, friends){
        Promise.map(friends, function(item){
          var result = _.pick(item, 'from', 'to');
          return _.values(result)
        }).then(function(Ids){
          return _.flatten(Ids);
        }).then(function(Ids){
          return _.uniq(Ids);
        }).then(function(Ids){
          resolve(Ids);
        });
      });
    });

    var timelinesP = friendsP.then(function(Ids){
      return Timeline.find().where({ owner: Ids }).populate('owner').sort({ createdAt: 'desc' })
    });
    
    var processTimelineP = timelinesP.then(function(results){
      return Promise.map(results, function(result){
        result = result.toJSON();
        var Model = sails.models[result.contextType];
        Model = Model.findOneById(result.contextId);

        return Model.then(function(contextResult){
          var obj = {};
          return Comment.find({postId: contextResult.id}).where({ sort: 'createdAt DESC' }).populate('owner').then(function(results){
            return Promise.map(results, function(result){
              result = result.toJSON();

              if(contextResult.owner === result.owner.id) {
                result.canDelete = true;
              } if(contextResult.owner !== result.owner.id && result.owner.id === req.user.id) {
                result.canDelete = true;
              } else {
                result.canDelete = false;
              }

              result.isMe = result.owner.id === uid ? true : false;
              return result;
            });
          }).then(function(comments){

            obj[result.contextType] = contextResult;
            obj[result.contextType].comments = comments;
            obj[result.contextType].isMe = contextResult.owner === req.user.id ? true : false;
            return _.extend(result, obj);
          });
        });
      });
    });

    var userP = User.findOneById(uid).then(function(user){
      user = user.toJSON();
      res.locals.userSelected = user;
      res.locals.isUserSelected = true;
      res.locals.notUpdateAvatar = true;
      res.locals.notUpdateProfile = true;
      return user;
    });

    var currentUserFriendsP = new Promise(function(resolve){
      Friend.getUserFriends(req.user.id, function(err, friends){
        Promise.map(friends, function(item){
          var result = _.pick(item, 'from', 'to');
          return _.values(result)
        }).then(function(Ids){
          return _.flatten(Ids);
        }).then(function(Ids){
          return _.uniq(Ids);
        }).then(function(Ids){
          return _.without(Ids, req.user.id);
        }).then(function(Ids){
          resolve(Ids);
        });
      });
    });

    Friend.findOne({
      or: [
        {from: req.user.id, to: uid},
        {from: uid, to: req.user.id}
      ]
    }).then(function(user){
      res.locals.isMyFriend = false;
      if(user && user.status === 'accepted') res.locals.isMyFriend = true;
      if(user && user.status === 'requested') res.locals.showDestroyFriend = true;
    });

    Promise.join(currentUserFriendsP, userP).spread(function(friends, user){
      res.locals.isMyFriend = _.contains(friends, user.id);
    });

    Promise.join(userP, processTimelineP).spread(function(user, timeline){
      res.render('profile/timeline', {
        model: timeline,
        hidePostStatus: true,
        showSendMessage: true,
        flash: req.flash('message')
      });
    });
  },

  delete: function(req, res) {
    if(!req.user.id) return res.forbiden();

    var uid = req.user.id;
    var postId = req.param('id');

    Timeline.findOne(postId).then(function(post){
      Timeline.destroy({id: post.id})
      .exec(function(err){
        if (err) return res.negotiate(err);

        // emit to user
        sails.io.sockets.emit('user %@ post'.fmt(uid), {
          id: post.id,
          verb: 'deleted',
          data: post
        });

        // send 200 response
        return res.ok(post);
      });
    });
  }
};

