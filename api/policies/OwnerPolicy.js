module.exports = function OwnerPolicy (req, res, next) {
  req.body = req.body || { };
  req.body.owner = req.user.id;

  next();
};
