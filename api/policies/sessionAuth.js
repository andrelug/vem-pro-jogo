/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {
  res.locals.currentUser = null;
  res.locals.authenticated = false;
  
  if (req.session.user) {
    res.locals.authenticated = true;
    User.findOneById(req.session.user).then(function(user){
      user = user.toJSON();
      req.user = user;
      res.locals.currentUser = user;
      res.locals.userSelected = user;
      if(!user) return res.redirect('/login');
      return next();
    });
  } else {
    return res.redirect('/login');
  }
  
  // return res.forbidden('You are not permitted to perform this action.');
};
