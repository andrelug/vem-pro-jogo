var bcrypt = require('bcrypt');

/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,

  attributes: {
    name: "string",
  
    registrationNumber: {
      type: 'string',
      required: true,
      notNull: true,
      unique: true,
      index: true
    },

    cpf: {
      type: 'string',
      required: true,
      notNull: true,
      unique: true,
      index: true
    },

    birth: 'date',
    email: {
      type: 'email',
      index: true
    },

    password: {
      type: 'string',
      required: true
    },

    phone: {
      type: 'string'
    },

    celphone: {
      type: 'string'
    },

    healthPlan: 'string',

    healthProblem: 'string',

    dateIn: 'date',

    dateOut: 'date',

    status: {
      type: 'string'
    },
    inicio: 'date',
    fim: 'date',
    uf: {
      type: 'string',
      index: true
    },
    area: {
      type: 'string',
      index: true
    },
    subarea: {
      type: 'string',
      index: true
    },

    healthInsuranceCard: {
      type: 'string'
    },

    healthPlanMode: {
      type: 'string'
    },

    healthPlanOperator: {
      type: 'string'
    },

    weight: {
      type: 'string'
    },

    height: {
      type: 'string'
    },

    bloodType: {
      type: 'string'
    },

    emergencyContact: {
      type: 'string'
    },

    medicalInformation: {
      type: 'string'
    },
    
    resetKey: {
      type: 'string'
    },

    gender: {
      type: 'string',
      enum: ['M', 'F']
    },
    avatar: "string",
    active: {
      type: 'boolean',
      defaultsTo: false
    },
    cover: {
      model: 'Photo'
    },
    posts: {
      collection: 'Post',
      via: 'owner',
      dominant: true
    },
    albuns: {
      collection: 'Album',
      via: 'owner',
      dominant: true
    },
    timelines: {
      collection: 'Timeline',
      via: 'owner',
      dominant: true
    },
    friends: {
      collection: 'Friend',
      via: 'to',
      dominant: true
    },
    photos: {
      collection: 'Photo',
      via: 'owner'
    },
    notifications: {
      collection: 'Notification',
      via: 'user'
    },
    projectParticipants: {
      collection: 'Participant',
      via: 'user'
    },
    role: {
      type: 'string',
      enum: ['admin', 'user']
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      obj.birthFormated = moment(obj.birth).format('DD/MM/YYYY');
      return obj;
    }
  },
  beforeCreate: function(user, cb) {
    user.role = user.role || 'user';
    user.avatar = user.avatar || '/assets/profile.png';
    user.active = user.active || false;
    
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          cb(err);
        } else {
          user.password = hash;
          cb(null, user);
        }
      });
    });
  },

  beforeUpdate: function(user, cb) {
    console.log(user.password, user.passwordComparison, user.password == user.passwordComparison);
    if(user.password == user.passwordComparison){
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(user.password, salt);
      user.password = hash;
    }

    cb(null, user);
  },

  // afterCreate: function(record, next){
  //   var userId = record.id;

  //   var timelineP = Album.create({ name: 'Timeline', owner: userId });
  //   var profileP = Album.create({ name: 'Profile', owner: userId });
  //   var coverP = Album.create({ name: 'Cover', owner: userId });
  //   var userP = User.findOneById(userId);

  //   var coverPhotoP = coverP.then(function(cover){
  //     return Photo.create({albumId: cover.id, owner: userId, img: 'images/profile-cover.jpg'});
  //   });

  //   var setCoverPhotoP = Promise.join(coverP, coverPhotoP).spread(function(cover, photo){
  //     cover.cover = photo.id;
  //     return cover.save();
  //   });

  //   var setUserCoverP = Promise.join(userP, coverPhotoP).spread(function(user, photo){
  //     user.cover = photo.id;
  //     return user.save();
  //   });

  //   return Promise.join(timelineP, profileP, setCoverPhotoP, setUserCoverP).then(function(){
  //     return next();
  //   });
  // }
};
