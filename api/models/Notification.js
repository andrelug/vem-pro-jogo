/**
* Notification.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,

  attributes: {
    contextType: {
      type: 'string',
      notNull: true
    },
    type: {
      type: 'string'
    },
    contextId: {
      type: 'string',
      notNull: true
    },
    text: {
      type: 'string'
    },
    read: {
      type: 'boolean',
      defaultsTo: false
    },
    owner:{
      model:'user'
    },
    user:{
      model:'user'
    },
  },
  beforeCreate: function(notification, cb) {
    notification.read = notification.read || false;
    cb(null, notification);
  }
};

