/**
* Message.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  
  attributes: {
    img: {
      type: 'string',
      notNull: true,
      required: true
    },
    url: {
      type: 'string'
    },
    owner:{
      model:'user'
    }
  },
  beforeCreate: function(record, cb) {
    record.url = record.url || '';

    cb(null, record);
  },
};
