/**
* Post.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  
  attributes: {
    text: 'string',
    photo: 'string',
    comments: {
      collection: 'Comment',
      via: 'postId',
      dominant: true
    },
    likes: {
      collection: 'PostLike',
      via: 'post',
      dominant: true
    },
    owner:{
      model:'user'
    }
  },

  afterCreate: function(record, cb){
    Timeline
      .create({ contextType: 'post', contextId: record.id, owner: record.owner })
      .then(function(result){
        Timeline.publishCreate(result);
        cb();
      });
  },
  afterDestroy: function(record, cb) {
    Comment.find({postId: record.id}).then(function(comments){
      return Promise.map(comments, function(comment){
        return comment.save();
      })
    }).then(function(){
      cb(null, record);
    });
  },
};

