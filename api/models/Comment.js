/**
* Comment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true, 

  attributes: {
    body: {
      type: 'string'
    },
    postId:{
      model:'post'
    },
    owner:{
      model:'user'
    },
    likes: {
      collection: 'CommentLike',
      via: 'comment',
      dominant: true
    },
    toJSON: function() {
      var obj = this.toObject();
      obj.formatedCreated = moment(obj.createdAt).format('LLL');
      return obj;
    }
  },

  afterCreate: function(record, next){

    var postP = Post.findOneById(record.postId);

    postP.then(function(post){
      if(post.owner !== record.owner){
        Notification
          .create({  type: 'comment', contextType: 'post', contextId: record.postId, owner: record.owner, user: post.owner })
          .then(function(result){
            sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
              id: result.id,
              verb: 'created',
              data: result
            });
            next();
          });
      } else {
        next();
      }
    });
  },

  afterDestroy: function(record, next){
    Notification
      .destroy({contextId: record[0].id})
      .then(function(result){
        sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
          id: result.id,
          verb: 'deleted',
          data: result
        });

        next();
      });
  },

};

