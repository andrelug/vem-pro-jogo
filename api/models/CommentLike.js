/**
* CommentLike.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    owner:{
      model:'user'
    },
    comment:{
      model:'comment'
    }
  },
  afterCreate: function(record, next){

    var commentP = Comment.findOneById(record.comment);

    commentP.then(function(comment){
      if(comment.owner !== record.owner){
        Notification
          .create({  type: 'commentLiked', contextType: 'post', contextId: comment.postId, owner: record.owner, user: comment.owner })
          .then(function(result){
            sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
              id: result.id,
              verb: 'created',
              data: result
            });
            next();
          });
      } else {
        next();
      }
    });
  },

  afterDestroy: function(record, next){
    record = record[0];

    Comment
      .findOneById(record.comment)
      .then(function(comment){
        Notification
          .destroy({type: 'commentLiked', contextId: comment.postId})
          .then(function(result){
            sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
              id: result.id,
              verb: 'deleted',
              data: result
            });
            next();
          });
      });
  },
};

