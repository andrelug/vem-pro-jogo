/**
 * Friend.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs    :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  schema: true,
  attributes: {

    // user how send the request
    from: {
      model:'user',
      required: true
    },

    to: {
      model:'user',
      required: true
    },

    // requested | accepted| ignored
    // requestsToYou
    status: {
      type: 'string',
      defaultsTo: 'requested',
      'in': ['requested', 'accepted', 'ignored']
    }
  },

  // Lifecycle Callbacks
  beforeCreate: function(record, next) {
    // on create status will be requested
    record.status = 'requested';
    next();
  },

  afterCreate: function(record, next){
    Notification
      .create({ type: 'friendRequest', contextType: 'friend', contextId: record.id, owner: record.from, user: record.to })
      .then(function(result){
        sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
          id: result.id,
          verb: 'created',
          data: result
        });
        next();
      });
  },

  afterUpdate: function(record, next){
    if(record.status === 'accepted') {
      Notification
        .create({ type: 'friendAccepted', contextType: 'friend', contextId: record.id, owner: record.to, user: record.from })
        .then(function(result){
          sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
            id: result.id,
            verb: 'created',
            data: result
          });
          next();
        });
    } else {
      next();
    }
  },

  afterDestroy: function(record, next){
    Notification
      .destroy({contextId: record[0].id})
      .then(function(result){
        sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
          id: result.id,
          verb: 'deleted',
          data: result
        });

        next();
      });
  },

  // beforeUpdate: function(record, next) {
  // },

  /**
   * Get user friend relationship
   * @param  {string}   uid      user id to get friends use for logged in user
   * @param  {string}   friendId      friend id
   * @param  {function} callback  after done exec with callback(error,friend)
   */
  getUsersRelationship: function(uid, friendId, callback){
    Friend.findOne({
      or: [
        { from: uid, to: friendId },
        { from: friendId, to: uid }
      ]
    })
    .exec(function (err, friend) {
      if(err) return callback(err, null);
      // no relationship found
      if(!friend) return callback();
      // if request is to user uid
      if(friend.status === 'requested' && friend.to === uid){
        friend.status = 'requestsToYou';
      }
      callback(err, friend);
    });
  },

  /**
   * Get user friends with user id
   * @param  {string}   uid      user id to get friends
   * @param  {function} callback  after done exec with callback(error,friends)
   */
  getUserFriends: function getUserFriends(uid, callback){
    Friend.find()
    .where({
      or: [{
        from: uid,
      },{
        to: uid
      }]
    })
    .exec(function(err, friends){
      callback(err,friends);
    });
  }
};
