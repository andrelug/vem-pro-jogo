/**
* Category.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
    name: {
      type: 'string',
      required: true,
      notNull: true
    },
    class: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string'
    },
    status: {
      type: 'boolean'
    },
    articles: {
      collection: 'Article',
      via: 'categoryId',
      dominant: true
    },
  }
};
