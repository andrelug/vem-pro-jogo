/**
* PostLike.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    owner:{
      model:'user'
    },
    post:{
      model:'post'
    }
  },
  afterCreate: function(record, next){

    var postP = Post.findOneById(record.post);

    postP.then(function(post){
      if(post.owner !== record.owner){
        Notification
          .create({ type: 'postLiked', contextType: 'post', contextId: record.post, owner: record.owner, user: post.owner })
          .then(function(result){
            sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
              id: result.id,
              verb: 'created',
              data: result
            });
            next();
          });
      } else {
        next();
      }
    });
  },

  afterDestroy: function(record, next){
    Notification
      .destroy({type: 'postLiked', contextId: record[0].id})
      .then(function(result){
        sails.io.sockets.emit('user %@ notification'.fmt(result.user), {
          id: result.id,
          verb: 'deleted',
          data: result
        });

        next();
      });
  },
};

