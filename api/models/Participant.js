/**
* Participants.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    user:{
      model:'user'
    },
    project:{
      model:'project'
    },
    status: {
      type: 'string',
      enum: ['Aceito', 'Convite', 'Pedido', 'Cancelado']
    },
  },
  beforeCreate: function(record, cb) {
    record.status = record.status || 'Convite';
    
    cb(null, record);
  },
  afterCreate: function(record, next){
    Project.findOneById(record.project)
      .then(function(result){
        if(record.status === 'Convite'){
          Notification
            .create({ type: 'inviteToProject', contextType: 'project', contextId: result.id, owner: result.owner, user: record.user })
            .then(function(result){
              sails.io.sockets.emit('user %@ notification'.fmt(record.user), {
                id: result.id,
                verb: 'created',
                data: result
              });
              next();
            });
        } else {
          next();
        }
      });
  }
};

