/**
* Article.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,

  types: {
    photo: function(photo){
      return {
        cover: photo.cover || '',
        normal: photo.normal || ''
      }
    }
  },

  attributes: {
    title: {
      type: 'string',
      required: true,
      notNull: true
    },
    subtitle: {
      type: 'string'
    },
    content: {
      type: 'string'
    },
    photo: {
      type: 'string'
    },
    status: {
      type: 'boolean'
    },
    categoryId: {
      model: 'category'
    }
  }
};
