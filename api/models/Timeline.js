/**
* Timeline.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  
  attributes: {
    contextType: {
      type: 'string',
      notNull: true
    },

    contextId: {
      type: 'string',
      notNull: true
    },

    owner:{
      model:'user'
    },
    toJSON: function() {
      var obj = this.toObject();
      obj.formatedCreated = moment(obj.createdAt).format('DD [de] MMMM [de] YYYY [as] HH:mm');
      return obj;
    }
  },

  afterDestroy: function(record, cb) {
    Post
      .destroy({id: record.contextId})
      .then(function(){
        cb(null, record);
      });
  },
};

