/**
* Project.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    city: {
      type: 'string'
    },
    state: {
      type: 'string'
    },
    lineOfAction: {
      type: 'string',
      required: true
    },
    startDate: {
      type: 'date',
      required: true
    },
    endDate: {
      type: 'date'
    },
    what: {
      type: 'string'
    },
    periodOfActivities: {
      type: 'string'
    },
    localOfRealization: {
      type: 'string'
    },
    suppliesNeeded: {
      type: 'string'
    },
    objective: {
      type: 'string'
    },
    costSpreadsheet: {
      type: 'string'
    },
    participants: {
      collection: 'Participant',
      via: 'project',
      dominant: true
    },
    secondaryTenderer:{
      model:'user'
    },

    maxParticipants: {
      type: 'string'
    },
    accept: {
      type: 'boolean'
    },
    owner:{
      model:'user'
    },
    status: {
      type: 'string',
      enum: ['Inscrito', 'Habilitado', 'Aprovado', 'Recusado', 'Em andamento', 'Encerrado', 'Descontinuado']
    },
    toJSON: function() {
      var obj = this.toObject();
      obj.startDateFormated = moment(obj.startDate).format('DD/MM/YY');
      obj.endDateFormated = moment(obj.endDate).format('DD/MM/YY');

      var statusObj = {
        'Inscrito': 'info',
        'Habilitado': 'info',
        'Aprovado': 'success',
        'Recusado': 'danger',
        'Em andamento': 'success',
        'Encerrado': 'danger',
        'Descontinuado':'default'
      }

      obj.statusClass = statusObj[obj.status];

      return obj;
    }
  },

  beforeValidate: function(project, cb) {
    project.startDate = moment(project.startDate)._d == 'Invalid Date' ? moment(project.startDate, 'DD/MM/YYYY')._d : project.startDate; 
    project.endDate = moment(project.endDate)._d == 'Invalid Date' ? moment(project.endDate, 'DD/MM/YYYY')._d : project.endDate;

    cb(null, project);
  },

  beforeUpdate: function(project, cb) {
    project.startDate = project.startDate && moment(project.startDate)._d;
    project.endDate = project.endDate && moment(project.endDate)._d;

    cb(null, project);
  },

  beforeCreate: function(project, cb) {
    project.status = project.status || 'Inscrito';

    cb(null, project);
  },
};
