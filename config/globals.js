/**
 * Global Variable Configuration
 * (sails.config.globals)
 *
 * Configure which global variables which will be exposed
 * automatically by Sails.
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.globals.html
 */
module.exports.globals = {
  _: true,
  async: true,
  sails: true,
  services: true,
  models: true
};

global.Promise = require('sails/node_modules/waterline/node_modules/bluebird');
global._ = require('sails/node_modules/lodash');
global.moment = require('moment');
global.gm = require('gm');
global.XLSX = require('xlsx-extract').XLSX;
global.pluckIds = _.partialRight(_.pluck, 'id');
global.actionUtil = require('../lib/actionUtil');

moment.locale('pt-BR');

String.prototype.fmt = function fmt() {
  // first, replace any ORDERED replacements.
  var args = arguments;

  var idx  = 0; // the current index for non-numerical replacements
  return this.replace(/%@([0-9]+)?/g, function(s, argIndex) {
    argIndex = (argIndex) ? parseInt(argIndex,0)-1 : idx++ ;
    s =args[argIndex];
    return ((s===null) ? '(null)' : (s===undefined) ? '' : s).toString(); 
  }) ;
}
