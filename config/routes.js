/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/*': function(req, res, next) {sails.log.info(req.method, req.url); next();},
  '/*': function(req, res, next) {
    var categories = Category.find().then(function(categories){
      res.locals.categories = categories;
    });

    var user = req.session.user && User.findOneById(req.session.user).then(function(user){
      user = user.toJSON();
      res.locals.currentUser = user;
      res.locals.userSelected = user;
      res.locals.authenticated = true;

      Notification.count({read: false, user: user.id}).then(function(unread){
        res.locals.notificationsUnread = unread;
      });
    });

    Promise.join(categories, user).nodeify(next);
  },

  'GET /': 'CategoryController.find',
  'GET /categoria/:id': 'CategoryController.findOne',
  'GET /noticia/:id': 'ArticleController.findOne',

  'POST /auth/login': 'AuthController.login',
  'GET /auth/logout': 'AuthController.logout',

  'GET /login': 'AuthController.view',

  'GET /password': 'UserController.forgotView',
  'POST /esqueci_senha': 'UserController.forgot',
  'GET /reset_password/:token': 'UserController.resetPasswordView',
  'POST /reset_password/:token': 'UserController.resetPassword',

  'GET /my-posts': 'TimelineController.myPosts',
  'GET /timeline': 'TimelineController.index',
  'GET /friend/:id/timeline': 'TimelineController.friendTimeline',
  'GET /friend/:id/about': 'ProfileController.friendAbout',
  'GET /friend/:id/friends': 'FriendController.viewOnlyFriends',
  'GET /friend/:id/projects': 'ProjectController.friendFind',

  'GET /post-likes/:id': 'PostController.likes',
  'GET /comment-likes/:id': 'CommentController.likes',

  'POST /comment/:id/like': 'CommentController.like',
  'POST /comment/:id/dislike': 'CommentController.dislike',

  'DELETE /post/:id': 'TimelineController.delete',
  'GET /post/:id': 'PostController.show',
  'GET /post/:id/img': 'PostController.img',
  'POST /post/:id/like': 'PostController.like',
  'POST /post/:id/dislike': 'PostController.dislike',

  'GET /about': 'ProfileController.about',

  'GET /friends': 'FriendController.find',
  'GET /onlyFriends': 'FriendController.onlyFriends',
  'POST /friends': 'FriendController.create',
  'PUT /friends/:id': 'FriendController.update',
  'PUT /friends/:id/accept': 'FriendController.accept',
  'DELETE /friends/:id': 'FriendController.delete',

  'DELETE /comment/:id': 'CommentController.delete',

  'GET /projects': 'ProjectController.index',
  'GET /projects_admin': 'ProjectController.find',
  'POST /projects': 'ProjectController.create',
  'POST /project/update': 'ProjectController.update',
  'GET /projects/:id': 'ProjectController.show',
  'POST /projects/:id/participants': 'ProjectController.participants',
  'POST /projects/:id/participants/:participantId/:action': 'ProjectController.participant',

  'GET /messages': 'MessageController.last',
  'GET /messages/:id': 'MessageController.list',
  'POST /messages': 'MessageController.create',

  'GET /notifications': 'NotificationController.find',

  'GET /admin': { view: 'admin/index', policy: 'adminPolicy' },

  'GET /admin/news': 'ArticleController.find',
  'POST /admin/news': 'ArticleController.create',
  'POST /admin/news/:id': 'ArticleController.update',
  'DELETE /admin/news/:id': 'ArticleController.delete',

  'GET /admin/projects': { view: 'admin/projects/index', policy: 'adminPolicy' },
  'GET /admin/projects/:id': 'ProjectController.adminFindOne',
  'GET /admin/projects/:id': 'ProjectController.adminFindOne',

  'GET /admin/newsticker': { view: 'admin/newsticker/index', policy: 'adminPolicy' },
  'POST /admin/newsticker': 'NewstickerController.create',
  'GET /admin/newsticker/find': 'NewstickerController.find',
  'DELETE /newsticker/:id': 'NewstickerController.delete',
  'GET /newsticker/:id/img': 'NewstickerController.img',

  'GET /admin/users': { view: 'admin/users/index', policy: 'adminPolicy' },
  'GET /admin/users/:id': 'UserController.adminFindOne',

  'GET /users': 'UserController.find',
  'GET /ativar-conta': 'UserController.profile',
  'POST /ativar-conta': 'UserController.active',
  'POST /user/avatar': 'UserController.avatar',

  'GET /profile/update': 'UserController.profile',
  'POST /profile/update': 'UserController.update',

  'GET /admin/categories': 'CategoryController.adminFind',


  'GET /faq': { view: 'faq' },

  'GET /contact': 'ContactController.index',
  'POST /contact': 'ContactController.create',
};
