$(function() {

	/*
	| -------------------------------------------------------------------------
	| sidebarProfile
	| -------------------------------------------------------------------------
	*/

	application.prototype.sidebarProfile = function () {
		var $this = this;

		$this.sidebarProfile.init = function() {
			// add custom validation for input file
			$.validator.addMethod("extension", function(value, element, param) {
				param = typeof param === "string" ? param.replace(/,/g, "|") : "png|jpe?g";
				return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
			}, $.validator.format("Please enter a value with a valid extension."));

			// form validation update profile picture
			$application.form.validate($('form#updateProfilePicture'), {
				'avatar': {
					required: true,
					extension: 'png|jpg|jpeg'
				}
			}, {
				'avatar': {
					required: 'Você precisa escolher uma imagem para atualizar!',
					extension: 'Você só pode enviar imagens em png ou jpg'
				}
			});

			// form validation for message
			$application.form.validate($('form#sendMessage'), {
				'content': {
					required: true
				}
			}, {
				'content': {
					required: 'Você não pode enviar uma mensagem em branco!'
				}
			});
		};

		$this.sidebarProfile.init();
	};
});