$(function() {

	/*
	| -------------------------------------------------------------------------
	| ui
	| -------------------------------------------------------------------------
	*/

	$('a').filter(function(){
		if($(this).attr('href').toLowerCase() === window.location.pathname.toLowerCase()) {
			return true;
		} else if($(this).attr('href').toLowerCase() === window.location.pathname.toLowerCase().replace(/^\/([^\/]*).*$/, '$1')) {
			return true;
		}
	}).parent().addClass('active');

	// bootstrap link to tab
	var url = document.location.toString();
	if (url.match('#')) {
	    $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
	    $("html, body").animate({
            scrollTop: 0
        }, 100);
	}

	// Change hash for page-reload
	$('.nav-tabs a').on('shown', function (e) {
	    window.location.hash = e.target.hash;
	})
});
