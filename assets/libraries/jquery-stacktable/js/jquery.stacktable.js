/**
 * stacktable.js
 * Author & copyright (c) 2012: John Polacek
 * Dual MIT & GPL license
 *
 * Page: http://johnpolacek.github.com/stacktable.js
 * Repo: https://github.com/johnpolacek/stacktable.js/
 *
 * jQuery plugin for stacking tables on small screens
 *
 */
;(function($) {

  $.fn.stacktable = function(options) {
    var $tables = this,
        defaults = {id:'stacktable',hideOriginal:false},
        settings = $.extend({}, defaults, options),
        stacktable;

    return $tables.each(function() {
      var $stacktable = $('<table class="'+settings.id + '"><tbody></tbody></table>');
      if (settings.class) $stacktable.addClass(settings.class);
      var markup = '';
      $table = $(this);
      $topRow = $table.find('tr').first();
      $table.find('tr').each(function(i,value) {

        // if($(value).attr('class') == undefined)
        // {
        //   $class = '';
        // }
        // else
        // {
        //   $class = 'class="' + $(value).attr('class') + '"';
        // }

        //markup += '<tr>'

        // for the first row, top left table cell is the head of the table
        // if (index===0) {
        //   markup += '<tr><th class="st-head-row st-head-row-main" colspan="2">'+$(this).find('th,td').first().html()+'</th></tr>';
        // }
        // for the other rows, put the left table cell as the head for that row
        // then iterate through the key/values
        //else {
          $(this).find('td').each(function(index,value) {
            // if (index===0) {
            //   markup += '<tr><th class="st-head-row" colspan="2">'+$(this).html()+'</th></tr>';
            // } else {

              
              if($(this).parent().attr('class') == undefined)
              {
                $class = '';
              }
              else
              {
                $class = ' class="' + $(this).parent().attr('class') + '"';
              }

              if ($(this).html() !== ''){
                markup += '<tr'+ $class +'>';

                  if($(this).attr('class') == undefined){
                    $class = '';
                  } else {
                    $class = $(this).attr('class');
                  }

                  if($topRow.find('td,th').eq(index).attr('class') == undefined) {
                    $topRowClass = '';
                  } else {
                    $topRowClass = $topRow.find('td,th').eq(index).attr('class');
                  }

                  console.log($topRow.find('th'))

                  markup += '<th class="' + $topRowClass + ' st-key">'+ $topRow.find('td,th').eq(index).html() + '</th>';
                  markup += '<td class="' + $class + ' st-val">'+$(this).html()+'</td>';


                
                markup += '</tr>';
              }
            //}
          });
        //}
      });
      $stacktable.append($(markup));
      $table.before($stacktable);
      //if (settings.hideOriginal) $table.hide();
    });
  };

}(jQuery));
