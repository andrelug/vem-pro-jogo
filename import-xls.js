new XLSX().extract('./import.xlsx', {
  sheet_nr:1,
  ignore_header: 1,
})
.on('row', function (row) {
    var newRow = _.zipObject(['registrationNumber', 'name', 'cpf', 'email', 'phone', 'birth', 'area', 'subarea', 'uf', 'dateIn', 'gender', 'idade', 'status', 'inicio', 'fim'], row);
    if(newRow.name && newRow.registrationNumber && newRow.cpf){
      newRow.email = isEmail(newRow.email) ? newRow.email.toLowerCase() : null;
      newRow.name = newRow.name.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
      newRow.phone = parseInt(newRow.phone) ? parseInt(newRow.phone) : null;

      row = _.extend(newRow, {password: newRow.cpf});

      User.findOne({registrationNumber: row.registrationNumber}).then(function(user){
        if(user){
          _.extend(user, row);
          // user.save().then(function(result){ console.log('update', result.registrationNumber); }).catch(console.log)
          user.save().catch(console.log)
        } else {
          User.create(row).catch(console.log);
        }
      });
    }
})
.on('error', function (err) {
    console.error('error', err);
})
.on('end', function (err) {
    console.log('eof');
});
