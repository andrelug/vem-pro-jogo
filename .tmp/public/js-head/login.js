$(function() {

	/*
	| -------------------------------------------------------------------------
	| timeline
	| -------------------------------------------------------------------------
	*/

	application.prototype.login = function () {
		var $this = this;

		$this.login.init = function() {
			// post
			$application.form.validate($('form#login'), {
				'username': {
					required: true
				},
				'password': {
					required: true
				}
			}, {
				'username': {
					required: 'Você precisa informar seu número de matrícula',
				},
				'password': {
					required: 'Você precisa informar sua senha ou CPF'
				}
			});
		};

		$this.login.init();
	};
});
