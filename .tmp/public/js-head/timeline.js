$(function() {

	/*
	| -------------------------------------------------------------------------
	| timeline
	| -------------------------------------------------------------------------
	*/

	application.prototype.timeline = function () {
		var $this = this;

		$this.timeline.init = function() {
			// add custom validation for input file
			$.validator.addMethod("extension", function(value, element, param) {
				param = typeof param === "string" ? param.replace(/,/g, "|") : "png|jpe?g";
				return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
			}, $.validator.format("Please enter a value with a valid extension."));

			// form validation post
			$application.form.validate($('form#post'), {
				'text': {
					required: true
				},
				'filePhoto': {
					extension: 'png|jpg|jpeg'
				}
			}, {
				'text': {
					required: 'Você precisa escrever algo para publicar'
				},
				'filePhoto': {
					extension: 'Você só pode enviar imagens em png ou jpg'
				}
			});

			$.each($('form#comment'), function() {
				$application.form.validate($(this), {
					'body': {
						required: true
					}
				}, {
					'body': {
						required: 'Você precisa escrever algo para comentar!'
					}
				});
			});

			// arruma o plural e singular da string "comentários" baseado no número de comentários.
			$('.comments-length').each(function(i, e) {
			  if(parseInt($(e).data('length')) == 1) {
			    $(e).text('Comentário');
			  } else {
			    $(e).text('Comentários');
			  }
			});

			// arruma o plural e singular da string "curtidas" baseado no número de curtidas.
			$('.likes-length').each(function(i, e) {
			  if(parseInt($(e).data('length')) == 1) {
			    $(e).text('Curtida');
			  } else {
			    $(e).text('Curtidas');
			  }
			});
		};

		$this.timeline.init();
	};
});