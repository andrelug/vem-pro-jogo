$(function() {

	/*
	| -------------------------------------------------------------------------
	| activeProfile
	| -------------------------------------------------------------------------
	*/

	application.prototype.activeProfile = function () {
		var $this = this;

		$this.activeProfile.init = function() {
			// treatment message for e-mails blank
			if($('form#activeProfile').find('input[name=email]').val() == '') {
				$('form#activeProfile').find('input[name=email]').parents('.form-group').append('<p class="help-block small">Você precisa criar o seu e-mail. Entre contato com o help desk.</p>');
			}

			// checkbox enable disable button
			$('form#activeProfile').find('button[type=submit]').attr('disabled', 'disabled');

			$('form#activeProfile').find('input[type=checkbox]').change(function() {
				if(this.checked) {
					$('form#activeProfile').find('button[type=submit]').removeAttr('disabled');
				} else {
					$('form#activeProfile').find('button[type=submit]').attr('disabled', 'disabled');
				}
			});

			// form validation activeProfile
			$application.form.validate($('form#activeProfile'), {
				'healthInsuranceCard': {
					required: true
				},
				'healthPlanMode': {
					required: true
				},
				'healthPlanOperator': {
					required: true
				},
				// 'weight': {
				// 	required: true
				// },
				// 'height': {
				// 	required: true
				// },
				// 'bloodType': {
				// 	required: true
				// },
				'emergencyContact': {
					required: true
				},
				'password': {
					required: true
				},
				'passwordComparison': {
					required: true,
					equalTo: "#password"
				}
			}, {
				'healthInsuranceCard': {
					required: 'Você precisa informar o número de cartão do seu plano de saúde'
				},
				'healthPlanMode': {
					required: 'Você precisa informar a modalidade do plano de saúde'
				},
				'healthPlanOperator': {
					required: 'Você precisa informar a nome da operadora do plano de saúde'
				},
				// 'weight': {
				// 	required: 'Você precisa informar seu peso'
				// },
				// 'height': {
				// 	required: 'Você precisa informar sua altura'
				// },
				// 'bloodType': {
				// 	required: 'Você precisa informar seu tipo sanguíneo'
				// },
				'emergencyContact': {
					required: 'Você precisa informar um contato para emergência'
				},
				'password': {
					required: 'Você precisa definir uma senha'
				},
				'passwordComparison': {
					required: 'Você precisa informar sua senha novamente',
					equalTo: 'As senhas informadas não são iguais'
				}
			});
		};

		$this.activeProfile.init();
	};
});