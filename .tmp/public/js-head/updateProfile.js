$(function() {

	/*
	| -------------------------------------------------------------------------
	| updateProfile
	| -------------------------------------------------------------------------
	*/

	application.prototype.updateProfile = function () {
		var $this = this;

		$this.updateProfile.init = function() {
			// treatment message for e-mails blank
			if($('form#updateProfile').find('input[name=email]').val() == '') {
				$('form#updateProfile').find('input[name=email]').parents('.form-group').append('<p class="help-block small">Você precisa criar o seu e-mail. Entre contato com o help desk.</p>');
			}

			// form validation updateProfile
			$application.form.validate($('form#updateProfile'), {
				'healthInsuranceCard': {
					required: true
				},
				'healthPlanMode': {
					required: true
				},
				'healthPlanOperator': {
					required: true
				},
				// 'weight': {
				// 	required: true
				// },
				// 'height': {
				// 	required: true
				// },
				// 'bloodType': {
				// 	required: true
				// },
				'emergencyContact': {
					required: true
				},
				'passwordComparison': {
					equalTo: "#password"
				}
			}, {
				'healthInsuranceCard': {
					required: 'Você precisa informar o número de cartão do seu plano de saúde'
				},
				'healthPlanMode': {
					required: 'Você precisa informar a modalidade do plano de saúde'
				},
				'healthPlanOperator': {
					required: 'Você precisa informar a nome da operadora do plano de saúde'
				},
				// 'weight': {
				// 	required: 'Você precisa informar seu peso'
				// },
				// 'height': {
				// 	required: 'Você precisa informar sua altura'
				// },
				// 'bloodType': {
				// 	required: 'Você precisa informar seu tipo sanguíneo'
				// },
				'emergencyContact': {
					required: 'Você precisa informar um contato para emergência'
				},
				'passwordComparison': {
					equalTo: 'As senhas informadas não são iguais'
				}
			});
		};

		$this.updateProfile.init();
	};
});