$(function() {

	/*
	| -------------------------------------------------------------------------
	| projects
	| -------------------------------------------------------------------------
	*/

	application.prototype.projects = function () {
		var $this = this;

		$this.projects.init = function() {

			// autocomplete states and cities
			new dgCidadesEstados({
			  cidade: document.getElementById('state'),
			  estado: document.getElementById('city')
			});

			// date range
			$('form#createProject').find('input[name=startDate]').datepicker({
			    format: "dd/mm/yyyy",
			    startDate: "01/11/2015",
			    endDate: "31/10/2018",
			    language: "pt-BR"
			});

			$('form#createProject').find('input[name=endDate]').datepicker({
			    format: "dd/mm/yyyy",
			    startDate: "01/11/2015",
			    endDate: "31/10/2018",
			    language: "pt-BR"
			});

			// checkbox enable disable button
			$('form#createProject').find('input[type=submit]').attr('disabled', 'disabled');

			$('form#createProject').find('input[type=checkbox]').change(function() {
				if(this.checked) {
					$('form#createProject').find('input[type=submit]').removeAttr('disabled');
				} else {
					$('form#createProject').find('input[type=submit]').attr('disabled', 'disabled');
				}
			});

			// add custom validation for participants
			$.validator.addMethod('participants', function(value, element) {
				if(value.length >= 3) {
					return true;
				} else {
					return false;
				}
			});

			// add custom validation for excludeSecondaryTenderer
			$.validator.addMethod('excludeSecondaryTenderer', function(value, element) {
				var result = true;
				$($('select[name=participants]').val()).each(function($i, $e){
					if($('select[name=secondaryTenderer]').val() == $e) {
						result = false;
					}
				});

				if(result == true) {
					return true;
				} else {
					return false;
				}
			});

			// add custom validation for maxParticipants
			$.validator.addMethod('maxParticipants', function(value, element) {
				if(parseInt($('form#createProject').find('input[name=maxParticipants]').val()) >= 5) {
					return true;
				} else {
					return false;
				}
			});



			// add custom validation for input file
			$.validator.addMethod("extension", function(value, element, param) {
				param = typeof param === "string" ? param.replace(/,/g, "|") : "png|jpe?g|gif";
				return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
			}, $.validator.format("Please enter a value with a valid extension."));

			// form validation createProject
			$application.form.validate($('form#createProject'), {
				'name': {
					required: true
				},
				'city': {
					required: true
				},
				'state': {
					required: true
				},
				'lineOfAction': {
					required: true
				},
				'startDate': {
					required: true
				},
				'endDate': {
					required: true
				},
				'what': {
					required: true
				},
				'periodOfActivities': {
					required: true
				},
				'localOfRealization': {
					required: true
				},
				'objective': {
					required: true
				},
				'costSpreadsheet': {
					required: true,
					extension: "xls|xlsx"
				},
				'secondaryTenderer': {
					required: true
				},
				'participants': {
					required: true,
					participants: true,
					excludeSecondaryTenderer: true
				},
				'maxParticipants': {
					required: true,
					number: true,
					min: 5,
					maxParticipants: true
				}
			}, {
				'name': {
					required: 'Você precisa informar o nome do seu projeto'
				},
				'city': {
					required: 'Você precisa informar a cidade em que seu projeto vai acontecer'
				},
				'state': {
					required: 'Você precisa informar o estado em que seu projeto vai acontecer'
				},
				'lineOfAction': {
					required: 'Você precisa informar a linha de atuação do seu projeto'
				},
				'startDate': {
					required: 'Você precisa informar a data de início do seu projeto'
				},
				'endDate': {
					required: 'Você precisa informar a data de fim do seu projeto'
				},
				'what': {
					required: 'Você precisa informar no que consiste seu projeto'
				},
				'periodOfActivities': {
					required: 'Você precisa informar a periodicidade das atividades do seu projeto'
				},
				'localOfRealization': {
					required: 'Você precisa informar onde serão realizadas as atividades do seu projeto'
				},
				'objective': {
					required: 'Você precisa informar o objetivo do seu projeto'
				},
				'costSpreadsheet': {
					required: 'Você precisa anexar a planilha orçamentaria devidamente preenchida',
					extension: 'Você só pode enviar planilhas com a extensão xls ou xlsx'
				},
				'secondaryTenderer': {
					required: 'Você precisa selecionar um proponente'
				},
				'participants': {
					required: 'Você precisa convidar um mínimo de 3 amigos',
					participants: 'Você precisa convidar um mínimo de 3 amigos',
					excludeSecondaryTenderer: 'Você não pode escolher como participante uma pessoa que já foi escolhida para ser o proponente 2, afinal, ela já é participante do seu projeto'
				},
				'maxParticipants': {
					required: 'Você precisa informar um número máximo de participantes',
					number: 'Você precisa informar um número',
					min: 'O número mínimo de participantes é 5. Lembre-se: o proponente 1 e 2 contam como participantes do projeto',
					maxParticipants: 'Você está convidando mais participantes do que o número máximo definido por você mesmo'
				}
			});
		};

		$this.projects.init();
	};
});
